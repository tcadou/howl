<?php
/** lib commune */
require '/opt/howl/lib/common.php';


$owConfig = new OwConfig();
$log_file = $owConfig->dir_output_mesure.'/ihm.log';

$owSensors = new OwSensors($owConfig, 'csv', $log_file);

?><!DOCTYPE html>
<html>
    
    <head>
        <title>howl</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.mobile-1.4.4.min.js"></script>
        <link href="css/jquery.mobile.structure-1.4.4.min.css" rel="stylesheet">
        <link href="css/jquery.mobile.theme-1.4.4.min.css" rel="stylesheet">

        <link href="css/widget.css" rel="stylesheet">

        <script type="text/javascript" src="js/dygraph-combined.js"></script>
        <!-- get var sunrisesunset -->
        <script type="text/javascript" src="get_sunrisesunset.php?nb_day=-1"></script>
        <script type="text/javascript">
        </script>
        <link href="css/graph.css" rel="stylesheet">
    </head>
    
    <body>
        <div data-role="page" id="now">
            <div data-role="header">
                <div data-role="navbar">
                    <ul>
                        <li>
                            <a data-role="button" class="ui-btn-active" href="#now" data-transition="none" data-mini="true">now</a>
                        </li>
                        <li>
                            <a data-role="button" href="#seconde" data-transition="flow" data-mini="true">G last 2 Days</a>
                        </li>
                        <li>
                            <a data-role="button" href="#day" data-transition="flow" data-mini="true">G last 15 Days</a>
                        </li>
                        <li>
                            <a data-role="button" href="#month" data-transition="flow" data-mini="true">G ALL</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div data-role="content">
                <div data-role="collapsible-set" id="mesure" data-mini="true">
                    <div data-role="collapsible" data-collapsed="false" data-mini="true">
                        <h1>Sensors</h1>
<?php
// affichage des widgets
foreach ($owSensors as $cur_owSensor) {
  if ($cur_owSensor->getDisplay(OwSensorAbstract::DISPLAY_WIDGET) === true) {
    echo $cur_owSensor->getHtmlWidget();
  }
}

?>
                    </div>
                    <div data-role="collapsible" data-mini="true">
                        <h1>switches</h1>
<?php
// affichage des widgets
foreach ($owSensors as $cur_owSensor) {
    if ($cur_owSensor->getType() === iOwSensor::TYPE_RELAIS) {
        echo '<fieldset>
            <div data-role="fieldcontain">
              <label for="select-based-flipswitch">'.$cur_owSensor->getAlias().'&nbsp;:</label>
              <select id="select-based-flipswitch" data-role="flipswitch" class="'.iOwSensor::TYPE_RELAIS.'" sensor_id="'.$cur_owSensor->getId().'">
                <option value="off">Off</option>
                <option value="on">On</option>
              </select>
            </div>
          </fieldset>';
    }
}
?>
                    </div>
                    <div data-role="collapsible">
                        <h1>info</h1>
                        <span>blablabla</span>
                    </div>
                </div>
            </div>
        </div>
        <div data-role="page" id="seconde">
            <div data-role="header">
                <div data-role="navbar">
                    <ul>
                        <li>
                            <a data-role="button" href="#now" data-transition="flow" data-mini="true">now</a>
                        </li>
                        <li>
                            <a data-role="button" class="ui-btn-active" href="#seconde" data-mini="true">G last 2 Days</a>
                        </li>
                        <li>
                            <a data-role="button" href="#day" data-transition="flow" data-mini="true">G last 15 Days</a>
                        </li>
                        <li>
                            <a data-role="button" href="#month" data-transition="flow" data-mini="true">G ALL</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div data-role="content">
                <div id="graph-seconde" class="graphmesure">Chargement...</div>
                <script type="text/javascript">
                $(document).on('pagecontainershow',function (e, ui) {
                  //console.log( "This page has just shown: " + ui.toPage[0].id );
                  if (ui.toPage[0].id == 'seconde') {

                     s = new Dygraph(
                        document.getElementById("graph-seconde"), // containing div
                        "get_mesure.php?periode=s", // CSV or path to a CSV file.
                        {
                            width: document.body.clientWidth-30, // todo essayer d'éviter cela !!! la classe graphmesure devrait suffire
                            height: document.body.clientHeight-100, //400,
                            underlayCallback: function(canvas, area, g) {
                              canvas.fillStyle = "rgba(220, 220, 220, 1.0)";

                              function highlight_period(x_start, x_end) {
                                var canvas_left_x = g.toDomXCoord(x_start);
                                var canvas_right_x = g.toDomXCoord(x_end);
                                var canvas_width = canvas_right_x - canvas_left_x;
                                canvas.fillRect(canvas_left_x, area.y, canvas_width, area.h);
                              }

                              // date de début du graph
                              var ts_start_date = g.getValue(0,0);
                              var o_start_date = new Date(ts_start_date);
                              var s_start_annee = o_start_date.getFullYear();
                              var s_start_mois = ('0'+(o_start_date.getMonth()+1)).slice(-2);
                              var s_start_jour = ('0'+o_start_date.getDate()).slice(-2);
                              var s_start_date = s_start_annee+'/'+s_start_mois+'/'+s_start_jour;

                              // date de fin du graph
                              var ts_end_date = g.getValue(g.numRows()-1,0);
                              var o_end_date = new Date(ts_end_date);
                              var s_end_annee = o_end_date.getFullYear();
                              var s_end_mois = ('0'+(o_end_date.getMonth()+1)).slice(-2);
                              var s_end_jour = ('0'+o_end_date.getDate()).slice(-2);
                              var s_end_date = s_end_annee+'/'+s_end_mois+'/'+s_end_jour;

                              // tant que nous n'arrivons pas à la date de fin du graph on gère chaque jour entre la date de début et la date de fin
                              var ts_cur_debut_date = ts_start_date;
                              var s_cur_date = s_start_date;
                              var o_cur_date = o_start_date;
                              var s_cur_annee = s_start_annee
                              var s_cur_mois = s_start_mois;
                              var s_cur_jour = s_start_jour;

                              o_condarret_date = o_end_date;
                              o_condarret_date.setDate(o_condarret_date.getDate()+1);
                              s_condarret_date = o_condarret_date.getFullYear()+'/'+
                                  ('0'+(o_condarret_date.getMonth()+1)).slice(-2)+'/'+
                                  ('0'+o_condarret_date.getDate()).slice(-2);
                              var i = 0;

                              while(s_cur_date != s_condarret_date) {
                                i = i+1;
                                if (sunrisesunset[s_cur_date] == null) {
                                  // pas de données pour cette date
                                  // calcul de la date suivante
                                  o_cur_date.setDate(o_cur_date.getDate()+1);
                                  s_cur_annee = o_cur_date.getFullYear();
                                  s_cur_mois = ('0'+(o_cur_date.getMonth()+1)).slice(-2);
                                  s_cur_jour = ('0'+o_cur_date.getDate()).slice(-2);
                                  s_cur_date = s_cur_annee+'/'+s_cur_mois+'/'+s_cur_jour;
                                  continue;
                                };

                                // ss
                                var o_cur_heure_ss = new String(sunrisesunset[s_cur_date].ss);
                                var cur_a_heure_ss = o_cur_heure_ss.split(':');
                                var o_ts_cur_heure_ss = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_ss[0], cur_a_heure_ss[1], 0);
                                //var ts_cur_heure_ss = o_ts_cur_heure_ss.getTime()/1000 - (o_ts_cur_heure_ss.getTimezoneOffset()*60) - 3600; // attention, il y a pb avec heure été et heure hiver

                                if (i == 1) {
                                  // sr
                                  var o_cur_heure_sr = new String(sunrisesunset[s_cur_date].sr);
                                  var cur_a_heure_sr = o_cur_heure_sr.split(':');
                                  var o_ts_cur_heure_sr = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_sr[0], cur_a_heure_sr[1], 0);
                                  if (ts_cur_debut_date < o_ts_cur_heure_sr.getTime()) {
                                    // on relie l'axe des y de gauche avec le sr de la première date elle est >
                                    highlight_period(ts_start_date, o_ts_cur_heure_sr.getTime());
                                  };
                                }else{
                                  // calcul de la date suivante
                                  o_cur_date.setDate(o_cur_date.getDate()+1);
                                  s_cur_annee = o_cur_date.getFullYear();
                                  s_cur_mois = ('0'+(o_cur_date.getMonth()+1)).slice(-2);
                                  s_cur_jour = ('0'+o_cur_date.getDate()).slice(-2);
                                  s_cur_date = s_cur_annee+'/'+s_cur_mois+'/'+s_cur_jour;

                                  if (sunrisesunset[s_cur_date] == null) {
                                    // nous avons ss mais pas sr du jour suivant, on utilise donc l'axe des ordonnées de droite
                                    highlight_period(o_ts_cur_heure_ss.getTime(), ts_end_date);
                                  }else{
                                    // sr
                                    var o_cur_heure_sr = new String(sunrisesunset[s_cur_date].sr);
                                    var cur_a_heure_sr = o_cur_heure_sr.split(':');
                                    var o_ts_cur_heure_sr = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_sr[0], cur_a_heure_sr[1], 0);
                                    //var ts_cur_heure_sr = o_ts_cur_heure_sr.getTime()/1000 - (o_ts_cur_heure_sr.getTimezoneOffset()*60) - 3600; // attention, il y a pb avec heure été et heure hiver

                                    highlight_period(o_ts_cur_heure_ss.getTime(), o_ts_cur_heure_sr.getTime());
                                  }
                                }
                              }
                            },

                            rollPeriod: 1,
                            rightGap: 20, // marge droite du graph
                            showRoller: true,
                            connectSeparatedPoints:true,
                            drawGapEdgePoints:true,
                            animatedZooms:true,
                            // pour ne pas afficher les courbes avec des labels spécifiques
                            drawCallback: function(dg, is_initial) {
                              if (!is_initial) return;
<?php
// affichage des courbes
foreach ($owSensors as $cur_owSensor) {
  if ($cur_owSensor->getDisplay(OwSensorAbstract::DISPLAY_GRAPH_SECONDE) === false) {
    echo 'dg.setVisibility(dg.indexFromSetName("'.$cur_owSensor->getAlias().'") - 1, 0);';
  }
}
?>
                            },
                            title: 'Mesures / seconde',
                            y2label: 'おんど (°C)',
                            ylabel: 'あかるさ / しつど (%)',
                            //series : {
                            //  'Light': {
                            //    axis: 'y2'
                            //  },
                            //  'Humidity': {
                            //    axis: 'y2'
                            //  }
                            //},
                            series : {
<?php
// affichage des courbes
foreach ($owSensors as $cur_owSensor) {
  if ($cur_owSensor->getType() != iOwSensor::TYPE_LUMINOSITE and $cur_owSensor->getType() != iOwSensor::TYPE_HUMIDITE and $cur_owSensor->getType() != iOwSensor::TYPE_RELAIS) {
    echo "'".$cur_owSensor->getAlias()."': { axis: 'y2' },";
  }
}
?>
                            },
                            legend: 'always',
                            axes: {
                                y: {
                                  valueRange: [0, 110]
                                },
                                y2: {
                                  strokeWidth: 2,
                                  independentTicks: true
                                },
                                x: {
                                    Ticker: Dygraph.dateTicker
                                }
                            },
                            stackedGraph: null,
                            highlightCircleSize: 4,
                            strokeWidth: 1,
                            strokeBorderWidth: 1, // null
                            highlightSeriesOpts: {
                              strokeWidth: 3,
                              strokeBorderWidth: 1,
                              highlightCircleSize: 5,
                            },
                        }
                    );
                  };
                });
                </script>
            </div>
        </div>
        <div data-role="page" id="day">
            <div data-role="header">
                <div data-role="navbar">
                    <ul>
                        <li>
                            <a data-role="button" href="#now" data-transition="flow" data-mini="true">now</a>
                        </li>
                        <li>
                            <a data-role="button" href="#seconde" data-transition="flow" data-mini="true">G last 2 Days</a>
                        </li>
                        <li>
                            <a data-role="button" class="ui-btn-active" href="#day" data-transition="none" data-mini="true">G last 15 Days</a>
                        </li>
                        <li>
                            <a data-role="button" href="#month" data-transition="flow" data-mini="true">G ALL</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div data-role="content">
                <div id="graph-minute" class="graphmesure">Chargement...</div>
                <script type="text/javascript">
                $(document).on('pagecontainershow',function (e, ui) {
                  //console.log( "This page has just shown: " + ui.toPage[0].id );
                  if (ui.toPage[0].id == 'day') {
                    m = new Dygraph(
                        document.getElementById("graph-minute"), // containing div
                        //"/data/mesure_seconde.csv", // CSV or path to a CSV file.
                        "get_mesure.php?periode=m", // CSV or path to a CSV file.
                        {
                            width: document.body.clientWidth-30, // todo essayer d'éviter cela !!! la classe graphmesure devrait suffire
                            height: document.body.clientHeight-100, //400,
                            underlayCallback: function(canvas, area, g) {
                              canvas.fillStyle = "rgba(220, 220, 220, 1.0)";

                              function highlight_period(x_start, x_end) {
                                var canvas_left_x = g.toDomXCoord(x_start);
                                var canvas_right_x = g.toDomXCoord(x_end);
                                var canvas_width = canvas_right_x - canvas_left_x;
                                canvas.fillRect(canvas_left_x, area.y, canvas_width, area.h);
                              }

                              // date de début du graph
                              var ts_start_date = g.getValue(0,0);
                              var o_start_date = new Date(ts_start_date);
                              var s_start_annee = o_start_date.getFullYear();
                              var s_start_mois = ('0'+(o_start_date.getMonth()+1)).slice(-2);
                              var s_start_jour = ('0'+o_start_date.getDate()).slice(-2);
                              var s_start_date = s_start_annee+'/'+s_start_mois+'/'+s_start_jour;

                              // date de fin du graph
                              var ts_end_date = g.getValue(g.numRows()-1,0);
                              var o_end_date = new Date(ts_end_date);
                              var s_end_annee = o_end_date.getFullYear();
                              var s_end_mois = ('0'+(o_end_date.getMonth()+1)).slice(-2);
                              var s_end_jour = ('0'+o_end_date.getDate()).slice(-2);
                              var s_end_date = s_end_annee+'/'+s_end_mois+'/'+s_end_jour;

                              // tant que nous n'arrivons pas à la date de fin du graph on gère chaque jour entre la date de début et la date de fin
                              var ts_cur_debut_date = ts_start_date;
                              var s_cur_date = s_start_date;
                              var o_cur_date = o_start_date;
                              var s_cur_annee = s_start_annee
                              var s_cur_mois = s_start_mois;
                              var s_cur_jour = s_start_jour;

                              o_condarret_date = o_end_date;
                              o_condarret_date.setDate(o_condarret_date.getDate()+1);
                              s_condarret_date = o_condarret_date.getFullYear()+'/'+
                                  ('0'+(o_condarret_date.getMonth()+1)).slice(-2)+'/'+
                                  ('0'+o_condarret_date.getDate()).slice(-2);
                              var i = 0;

                              while(s_cur_date != s_condarret_date) {
                                i = i+1;
                                if (sunrisesunset[s_cur_date] == null) {
                                  // pas de données pour cette date
                                  // calcul de la date suivante
                                  o_cur_date.setDate(o_cur_date.getDate()+1);
                                  s_cur_annee = o_cur_date.getFullYear();
                                  s_cur_mois = ('0'+(o_cur_date.getMonth()+1)).slice(-2);
                                  s_cur_jour = ('0'+o_cur_date.getDate()).slice(-2);
                                  s_cur_date = s_cur_annee+'/'+s_cur_mois+'/'+s_cur_jour;
                                  continue;
                                };

                                // ss
                                var o_cur_heure_ss = new String(sunrisesunset[s_cur_date].ss);
                                var cur_a_heure_ss = o_cur_heure_ss.split(':');
                                var o_ts_cur_heure_ss = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_ss[0], cur_a_heure_ss[1], 0);
                                //var ts_cur_heure_ss = o_ts_cur_heure_ss.getTime()/1000 - (o_ts_cur_heure_ss.getTimezoneOffset()*60) - 3600; // attention, il y a pb avec heure été et heure hiver

                                if (i == 1) {
                                  // sr
                                  var o_cur_heure_sr = new String(sunrisesunset[s_cur_date].sr);
                                  var cur_a_heure_sr = o_cur_heure_sr.split(':');
                                  var o_ts_cur_heure_sr = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_sr[0], cur_a_heure_sr[1], 0);
                                  if (ts_cur_debut_date < o_ts_cur_heure_sr.getTime()) {
                                    // on relie l'axe des y de gauche avec le sr de la première date elle est >
                                    highlight_period(ts_start_date, o_ts_cur_heure_sr.getTime());
                                  };
                                }else{
                                  // calcul de la date suivante
                                  o_cur_date.setDate(o_cur_date.getDate()+1);
                                  s_cur_annee = o_cur_date.getFullYear();
                                  s_cur_mois = ('0'+(o_cur_date.getMonth()+1)).slice(-2);
                                  s_cur_jour = ('0'+o_cur_date.getDate()).slice(-2);
                                  s_cur_date = s_cur_annee+'/'+s_cur_mois+'/'+s_cur_jour;

                                  if (sunrisesunset[s_cur_date] == null) {
                                    // nous avons ss mais pas sr du jour suivant, on utilise donc l'axe des ordonnées de droite
                                    highlight_period(o_ts_cur_heure_ss.getTime(), ts_end_date);
                                  }else{
                                    // sr
                                    var o_cur_heure_sr = new String(sunrisesunset[s_cur_date].sr);
                                    var cur_a_heure_sr = o_cur_heure_sr.split(':');
                                    var o_ts_cur_heure_sr = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_sr[0], cur_a_heure_sr[1], 0);
                                    //var ts_cur_heure_sr = o_ts_cur_heure_sr.getTime()/1000 - (o_ts_cur_heure_sr.getTimezoneOffset()*60) - 3600; // attention, il y a pb avec heure été et heure hiver

                                    highlight_period(o_ts_cur_heure_ss.getTime(), o_ts_cur_heure_sr.getTime());
                                  }
                                }
                              }
                            },

                            rollPeriod: 1,
                            rightGap: 20, // marge droite du graph
                            showRoller: true,
                            connectSeparatedPoints:false,
                            drawGapEdgePoints:true,
                            //axisLabelFontSize:10,
                            animatedZooms:true,
                            //  errorBars: true,
                            // pour ne pas afficher les courbes avec des labels spécifiques
                            drawCallback: function(dg, is_initial) {
                              if (!is_initial) return;
<?php
// affichage des courbes
foreach ($owSensors as $cur_owSensor) {
  if ($cur_owSensor->getDisplay(OwSensorAbstract::DISPLAY_GRAPH_MINUTE) === false) {
    echo 'dg.setVisibility(dg.indexFromSetName("'.$cur_owSensor->getAlias().'") - 1, 0);';
  }
}
?>
                            },
                            //dateWindow:[1274313600400, 1274313601631], // permet de définir un zoom initiale
                            //clickCallback: function(e, x, pts) {
                            //    function toogleFullscreen() {
                            //        if ( this.fullscreen == 0){ // || typeof this.fullscreen == 'undefined'
                            //            this.fullscreen = 1;
                            //        }else{
                            //            this.fullscreen = 0;
                            //        }
                            //        return this.fullscreen;
                            //    }
                            //    if (toogleFullscreen() == 0) {
                            //        t.resize(800, 400);
                            //    }else{
                            //        t.resize(document.body.clientWidth-30, document.body.clientHeight-60);
                            //    }
                            //},
                            title: 'Mesures / minute',
                            //labels: ["Date","Série 1","Série 2"],
                            y2label: 'おんど (°C)',
                            ylabel: 'あかるさ / しつど (%)',
                            series : {
<?php
// affichage des courbes
foreach ($owSensors as $cur_owSensor) {
  if ($cur_owSensor->getType() != iOwSensor::TYPE_LUMINOSITE and $cur_owSensor->getType() != iOwSensor::TYPE_HUMIDITE and $cur_owSensor->getType() != iOwSensor::TYPE_RELAIS) {
    echo "'".$cur_owSensor->getAlias()."': { axis: 'y2' },";
  }
}
?>
                            },
                            legend: 'always',
                            //labelsSeparateLines: true,
                            stackedGraph: null,
                            highlightCircleSize: 4,
                            strokeWidth: 1,
                            strokeBorderWidth: 1, // null
                            highlightSeriesOpts: {
                              strokeWidth: 3,
                              strokeBorderWidth: 1,
                              highlightCircleSize: 5,
                            },
                            axes: {
                                y: {
                                  valueRange: [0, 110]
                                },
                                y2: {
                                  //drawGrid: true,
                                  independentTicks: true,
                                  strokeWidth: 2
                                  //labelsKMB: false,
                                  //ticker: Dygraph.numericLinearTicks,
                                  //gridLineColor: "#ff0000",
                                  //gridLinePattern: [4,4] // [2,2]
                                },
                                x: {
                                    //valueRange: [10,50], // ?????
                                    //valueFormatter: Dygraph.dateString_,
                                    Ticker: Dygraph.dateTicker
                                }
                            }
                        }
                    );
                  };
                });
                </script>
            </div>
        </div>
        <div data-role="page" id="month">
            <div data-role="header">
                <div data-role="navbar">
                    <ul>
                        <li>
                            <a data-role="button" href="#now" data-transition="flow" data-mini="true">now</a>
                        </li>
                        <li>
                            <a data-role="button" href="#seconde" data-transition="flow" data-mini="true">G last 2 Days</a>
                        </li>
                        <li>
                            <a data-role="button" href="#day" data-transition="flow" data-mini="true">G last 15 Days</a>
                        </li>
                        <li>
                            <a data-role="button" class="ui-btn-active" href="#month" data-transition="none" data-mini="true">G ALL</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div data-role="content">
                <div id="graph-heure" class="graphmesure">Chargement...</div>
                <script type="text/javascript">
                $(document).on('pagecontainershow',function (e, ui) {
                  //console.log( "This page has just shown: " + ui.toPage[0].id );
                  if (ui.toPage[0].id == 'month') {
                    h = new Dygraph(
                        document.getElementById("graph-heure"), // containing div
                        "get_mesure.php?periode=h&nb_point=-1", // CSV or path to a CSV file.
                        {
                            width: document.body.clientWidth-30, // todo essayer d'éviter cela !!! la classe graphmesure devrait suffire
                            height: document.body.clientHeight-100, //400,
                            customBars: true, //  à activer pour les graphs HEURE
                            underlayCallback: function(canvas, area, g) {
                              canvas.fillStyle = "rgba(220, 220, 220, 1.0)";

                              function highlight_period(x_start, x_end) {
                                var canvas_left_x = g.toDomXCoord(x_start);
                                var canvas_right_x = g.toDomXCoord(x_end);
                                var canvas_width = canvas_right_x - canvas_left_x;
                                canvas.fillRect(canvas_left_x, area.y, canvas_width, area.h);
                              }

                              // date de début du graph
                              var ts_start_date = g.getValue(0,0);
                              var o_start_date = new Date(ts_start_date);
                              var s_start_annee = o_start_date.getFullYear();
                              var s_start_mois = ('0'+(o_start_date.getMonth()+1)).slice(-2);
                              var s_start_jour = ('0'+o_start_date.getDate()).slice(-2);
                              var s_start_date = s_start_annee+'/'+s_start_mois+'/'+s_start_jour;

                              // date de fin du graph
                              var ts_end_date = g.getValue(g.numRows()-1,0);
                              var o_end_date = new Date(ts_end_date);
                              var s_end_annee = o_end_date.getFullYear();
                              var s_end_mois = ('0'+(o_end_date.getMonth()+1)).slice(-2);
                              var s_end_jour = ('0'+o_end_date.getDate()).slice(-2);
                              var s_end_date = s_end_annee+'/'+s_end_mois+'/'+s_end_jour;

                              // tant que nous n'arrivons pas à la date de fin du graph on gère chaque jour entre la date de début et la date de fin
                              var ts_cur_debut_date = ts_start_date;
                              var s_cur_date = s_start_date;
                              var o_cur_date = o_start_date;
                              var s_cur_annee = s_start_annee
                              var s_cur_mois = s_start_mois;
                              var s_cur_jour = s_start_jour;

                              o_condarret_date = o_end_date;
                              o_condarret_date.setDate(o_condarret_date.getDate()+1);
                              s_condarret_date = o_condarret_date.getFullYear()+'/'+
                                  ('0'+(o_condarret_date.getMonth()+1)).slice(-2)+'/'+
                                  ('0'+o_condarret_date.getDate()).slice(-2);
                              var i = 0;

                              while(s_cur_date != s_condarret_date) {
                                i = i+1;
                                if (sunrisesunset[s_cur_date] == null) {
                                  // pas de données pour cette date
                                  // calcul de la date suivante
                                  o_cur_date.setDate(o_cur_date.getDate()+1);
                                  s_cur_annee = o_cur_date.getFullYear();
                                  s_cur_mois = ('0'+(o_cur_date.getMonth()+1)).slice(-2);
                                  s_cur_jour = ('0'+o_cur_date.getDate()).slice(-2);
                                  s_cur_date = s_cur_annee+'/'+s_cur_mois+'/'+s_cur_jour;
                                  continue;
                                };

                                // ss
                                var o_cur_heure_ss = new String(sunrisesunset[s_cur_date].ss);
                                var cur_a_heure_ss = o_cur_heure_ss.split(':');
                                var o_ts_cur_heure_ss = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_ss[0], cur_a_heure_ss[1], 0);
                                //var ts_cur_heure_ss = o_ts_cur_heure_ss.getTime()/1000 - (o_ts_cur_heure_ss.getTimezoneOffset()*60) - 3600; // attention, il y a pb avec heure été et heure hiver

                                if (i == 1) {
                                  // sr
                                  var o_cur_heure_sr = new String(sunrisesunset[s_cur_date].sr);
                                  var cur_a_heure_sr = o_cur_heure_sr.split(':');
                                  var o_ts_cur_heure_sr = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_sr[0], cur_a_heure_sr[1], 0);
                                  if (ts_cur_debut_date < o_ts_cur_heure_sr.getTime()) {
                                    // on relie l'axe des y de gauche avec le sr de la première date elle est >
                                    highlight_period(ts_start_date, o_ts_cur_heure_sr.getTime());
                                  };
                                }else{
                                  // calcul de la date suivante
                                  o_cur_date.setDate(o_cur_date.getDate()+1);
                                  s_cur_annee = o_cur_date.getFullYear();
                                  s_cur_mois = ('0'+(o_cur_date.getMonth()+1)).slice(-2);
                                  s_cur_jour = ('0'+o_cur_date.getDate()).slice(-2);
                                  s_cur_date = s_cur_annee+'/'+s_cur_mois+'/'+s_cur_jour;

                                  if (sunrisesunset[s_cur_date] == null) {
                                    // nous avons ss mais pas sr du jour suivant, on utilise donc l'axe des ordonnées de droite
                                    highlight_period(o_ts_cur_heure_ss.getTime(), ts_end_date);
                                  }else{
                                    // sr
                                    var o_cur_heure_sr = new String(sunrisesunset[s_cur_date].sr);
                                    var cur_a_heure_sr = o_cur_heure_sr.split(':');
                                    var o_ts_cur_heure_sr = new Date(s_cur_annee, s_cur_mois -1, s_cur_jour, cur_a_heure_sr[0], cur_a_heure_sr[1], 0);
                                    //var ts_cur_heure_sr = o_ts_cur_heure_sr.getTime()/1000 - (o_ts_cur_heure_sr.getTimezoneOffset()*60) - 3600; // attention, il y a pb avec heure été et heure hiver

                                    highlight_period(o_ts_cur_heure_ss.getTime(), o_ts_cur_heure_sr.getTime());
                                  }
                                }
                              }
                            },
                            
                            rollPeriod: 20,
                            rightGap: 20, // marge droite du graph
                            showRoller: true,
                            connectSeparatedPoints:false,
                            drawGapEdgePoints:true,
                            animatedZooms:true,
                            // pour ne pas afficher les courbes avec des labels spécifiques
                            drawCallback: function(dg, is_initial) {
                              if (!is_initial) return;
<?php
// affichage des courbes
foreach ($owSensors as $cur_owSensor) {
  if ($cur_owSensor->getDisplay(OwSensorAbstract::DISPLAY_GRAPH_HEURE) === false) {
    echo 'dg.setVisibility(dg.indexFromSetName("'.$cur_owSensor->getAlias().'") - 1, 0);';
  }
}
?>
                            },
                            title: 'Mesures / hour',
                            y2label: 'おんど (°C)',
                            ylabel: 'あかるさ / しつど (%)',
                            series : {
<?php
// affichage des courbes
foreach ($owSensors as $cur_owSensor) {
  if ($cur_owSensor->getType() != iOwSensor::TYPE_LUMINOSITE and $cur_owSensor->getType() != iOwSensor::TYPE_HUMIDITE and $cur_owSensor->getType() != iOwSensor::TYPE_RELAIS) {
    echo "'".$cur_owSensor->getAlias()."': { axis: 'y2' },";
  }
}
?>
                            },
                            legend: 'always',
                            stackedGraph: null,
                            highlightCircleSize: 4,
                            strokeWidth: 1,
                            strokeBorderWidth: 1, // null
                            highlightSeriesOpts: {
                              strokeWidth: 3,
                              strokeBorderWidth: 1,
                              highlightCircleSize: 5,
                            },
                            axes: {
                                y: {
                                  valueRange: [0, 110]
                                },
                                y2: {
                                  strokeWidth: 5,
                                  independentTicks: true
                                },
                                x: {
                                    Ticker: Dygraph.dateTicker
                                }
                            },
                            stackedGraph: null,
                            highlightCircleSize: 4,
                            strokeWidth: 1,
                            strokeBorderWidth: 1, // null
                            highlightSeriesOpts: {
                              strokeWidth: 3,
                              strokeBorderWidth: 1,
                              highlightCircleSize: 5,
                            }
                        }
                    );
                  };
                });
                </script>
            </div>
        </div>
        <script type="text/javascript" src="js/interaction.js.php"></script>
    </body>
</html>