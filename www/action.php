<?php
/**
 * réalise les actions pour les sensors (status et changement d'état)
 */
/** lib commune */
require '/opt/howl/lib/common.php';

header('Content-type: application/json');

/** Initialisation des variables */
$owConfig = new OwConfig();
$log_file = $owConfig->dir_output_mesure.'/ihm.log';

$owNet = new OWNet("tcp://".$owConfig->owserver_ip.":".$owConfig->owserver_port);

if (empty($_GET['action']) or empty($_GET['sensor_id']) or empty($_GET['sensor_type'])) {
	echo '{"paramètres invalides"}';
	exit;
}

// lecture des sondes
if ($_GET['action'] == 'read' and !empty($_GET['sensor_id']) and !empty($_GET['sensor_type'])) {
	foreach ($owConfig->sensors[$_GET['sensor_id']] as $cur_sensor_conf) {
		if ($cur_sensor_conf['type'] == $_GET['sensor_type']) {
			if (empty($cur_sensor_conf['correction'])) {
				$cur_sensor_conf['correction'] = '';
			}
			$cur_owSensor = OwSensorFactory::getSensor(
			    $owConfig,
			    'sensors',
			    $_GET['sensor_id'], 
			    $cur_sensor_conf['type'], 
			    $cur_sensor_conf['alias'],
			    $owConfig->separateur_decimal,
			    null,
			    $cur_sensor_conf['display'],
			    $cur_sensor_conf['correction']
			);
			// getMesure retourne la mesure "instantanée" pour la téléinfo. pour le reste, c'est la mesure normale
			$mesure = $cur_owSensor->getMesure();
			if (is_array($mesure)) {
				$mesure = $mesure[BatchMesure::CHANGEMENT_SECONDE];
			}
			echo '{"sensor_id":"'.$_GET['sensor_id'].'","value":"'.$mesure.'"}';
			exit;
		}
	}
}


// ecriture des sondes
if ($_GET['action'] == 'write' and !empty($_GET['sensor_id']) and isset($_GET['value']) and !empty($_GET['sensor_type']) and $_GET['sensor_type'] == iOwSensor::TYPE_RELAIS) {
	foreach ($owConfig->sensors[$_GET['sensor_id']] as $cur_sensor_conf) {
		if ($cur_sensor_conf['type'] == $_GET['sensor_type']) {
			if (empty($cur_sensor_conf['correction'])) {
				$cur_sensor_conf['correction'] = '';
			}
			$cur_owSensor = OwSensorFactory::getSensor(
			    $owConfig,
			    'sensors',
			    $_GET['sensor_id'], 
			    $cur_sensor_conf['type'], 
			    $cur_sensor_conf['alias'],
			    $owConfig->separateur_decimal,
			    null,
			    $cur_sensor_conf['display'],
			    $cur_sensor_conf['correction']
			);
			if (empty($_GET['value'])) {
				echo '{"paramètres invalides pour setter un relais"}';
				exit;
			}
			if ($_GET['value'] == 'off') {
				$cur_owSensor->open();
			}else{
				$cur_owSensor->close();
			}
			echo '{"sensor_id":"'.$_GET['sensor_id'].'","value":"'.$cur_owSensor->getMesure().'"}';
			exit;
		}
	}
}

echo '{"action inconnue"}';
exit;