<?php
/** lib commune */
require '/opt/howl/lib/common.php';
header("Content-type: application/x-javascript");
?>
/*
GRAPH
	Sur cet événement show( event, ui )
		http://api.jquerymobile.com/pagecontainer/#event-show
		déclencher l'affichage du graph de la page que l'on va afficher
*/

$(document).on('pagecontainershow',function (e, ui) {
	//console.log( "This page has just shown: " + ui.toPage[0].id );
	if (ui.toPage[0].id == 'now') {
	  	// Pour chaque relais, on récupère son status
	  	$( "select.<?php echo iOwSensor::TYPE_RELAIS; ?>[data-role='flipswitch']" ).each(function( index ) {
			$.getJSON( "/action.php?action=read&sensor_id="+$(this).attr('sensor_id')+"&sensor_type=<?php echo iOwSensor::TYPE_RELAIS; ?>", function( data ) {
				if (data.value == 0) {
					// OFF
					$("select[sensor_id='"+data.sensor_id+"']").val('off').flipswitch('refresh');
				}else{
					// ON
					$("select[sensor_id='"+data.sensor_id+"']").val('on').flipswitch('refresh');
				};
			});
		});
	};
});


// Pour chaque relais, on relie le bouton avec le relais
$( "select.<?php echo iOwSensor::TYPE_RELAIS; ?>[data-role='flipswitch']" ).on('change',function () {
	$.getJSON( "/action.php?action=write&sensor_id="+$(this).attr('sensor_id')+"&sensor_type=<?php echo iOwSensor::TYPE_RELAIS; ?>&value="+$("select[sensor_id='"+$(this).attr('sensor_id')+"']").val(), function( data ) {
		if (data.value == 0) {
			// OFF
			$("select[sensor_id='"+data.sensor_id+"']").val('off').flipswitch('refresh');
		}else{
			// ON
			$("select[sensor_id='"+data.sensor_id+"']").val('on').flipswitch('refresh');
		};
	});
});

