<?php
/**
 * récupère les heures de sunrise et sunset pour les n derniers jours et les stock dans une variable JS
 *
 * Usage : http://127.0.0.1/get_sunrisesunset.php?nb_day=2
 * nb_day=-1 pour obtenir toutes les données
 */

if (!empty($_GET['nb_day'])){
	$block_ownet_check = true; // désactive le check et l'inclusion de la lib ownet car elle n'est pas nécessaire pour ce programme en mode get
	$GLOBALS['nb_day'] = $_GET['nb_day'];
	require '/opt/howl/lib/common.php';
	header('Content-Type: application/javascript; charset=UTF-8');
}elseif (empty($GLOBALS['nb_day'])) {
	die('');
}

// conf
if (empty($owConfig)) {
	$owConfig = new OwConfig();
}
	
$fichier_sunrisesunset = $owConfig->dir_output_mesure.'/'.$owConfig->fichier_sunrisesunset;

$sunriseSunset = new SunriseSunset(
	$owConfig->location_sunrisesunset,
	$fichier_sunrisesunset
);
echo "var sunrisesunset = ".$sunriseSunset->load($GLOBALS['nb_day']).";\n";
