<?php
/**
 * récupère les Mesure au format CSV pour une fréquence donnée
 *
 * Usage : 
 * - http://127.0.0.1/get_mesure.php
 * - ?periode=s|m|h // seconde|minute|seconde
 * - &nb_point=XXXX // nombre de ligne a retourner. si vide on prend la valeur du fichier de config corespondant à la période.
 */

$block_ownet_check = true; // désactive le check et l'inclusion de la lib ownet car elle n'est pas nécessaire pour ce programme
require '/opt/howl/lib/common.php';

header('Content-Type: text/csv; charset=UTF-8');

if (empty($_GET['periode'])) {
	die('');
}

$owConfig = new OwConfig();

switch ($_GET['periode']) {
	case 's':
		// some cache
		header("Cache-Control: max-age=60"); // 1 minute
		if (empty($_GET['nb_point'])) {
			$_GET['nb_point'] = $owConfig->max_point_graph_seconde;
		}
		$file = $owConfig->dir_output_mesure.'/'.$owConfig->fichier_mesure_seconde;
		break;
	case 'm':
		// some cache
		header("Cache-Control: max-age=600"); // 10 minutes
		if (empty($_GET['nb_point'])) {
			$_GET['nb_point'] = $owConfig->max_point_graph_minute;
		}
		$file = $owConfig->dir_output_mesure.'/'.$owConfig->fichier_mesure_minute;
		break;
	case 'h':
		// some cache
		header("Cache-Control: max-age=1800"); // 30 minutes
		if (empty($_GET['nb_point'])) {
			$_GET['nb_point'] = $owConfig->max_point_graph_heure;
		}
		$file = $owConfig->dir_output_mesure.'/'.$owConfig->fichier_mesure_heure;
		break;	
	default:
		break;
}

if (empty($_GET['nb_point']) or $_GET['nb_point'] < 0) {
	// on retourne tout le fichier
	readfile($file);
	exit;
}

$nb_line = trim(exec('wc -l '.$file));
if ($nb_line <= $_GET['nb_point']) {
	// on retourne tout le fichier car il y a moins de ligne que demandées
	readfile($file);
	exit;
}

$mesureCsv = new MesureCsv($owConfig);

echo implode(',', $mesureCsv->loadHeader())."\n";
passthru('tail -n '.$_GET['nb_point'].' '.$file);
