<?php
/** Vous pouvez redéfinir la timezone selon votre localisation */
date_default_timezone_set('Europe/Paris');

/**
 * Config personnalisée
 * Pour déclarer vos propres paramètres et sondes, il faut déclarer la variable d'environnement HOWL_CONFIG et préciser 
 * le nom du fichier de config en incluant le chemin complet. Il sera chargé à la place de la classe OwConfig ci-dessous
 *
 * @warning Les mesures dans les fichiers CSV sont dans le même ordre que les déclarations des sondes.
 * Penser à toujours ajouter les nouvelles sondes à la fin
 * l'ordre des sondes ne doit jamais être modifiée et aucune sonde ne doit être supprimée.
 * Il en résulterait une incohérence entre les entêtes des fichiers CSV et les mesures ainsi qu'entre les anciennes 
 * et les nouvelles mesures.
 * Pour ne plus voir appraître une sonde (qu'elle existe ou non sur le réseau) il faut la "désactiver" (voir le paramètre "display")
 * @param "display" permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
 * pour désactiver entièrement l'affichage d'une sonde, il faut que 'display' soit égale à un tableau vide.
 * @param "correction" c'est une formule mathématique ou "X" sera remplacée par la mesure de la sonde.
 * attention, la correction n'est pas directement appliquée à la mesure de la sonde mais aux chiffres stockés dans les CSV.
 * @param "accuracy" La précision est égale à OwSensorAbstract::ACCURACY_DISPLAY OwSensorAbstract::ACCURACY_ACCURATE 
 * par défaut c'est OwSensorAbstract::ACCURACY_DISPLAY. L'utilisation de OwSensorAbstract::ACCURACY_DISPLAY permet de 
 * stocker les mêmes chiffres dans les CSV que ceux qui sont affichés sur les graph. Clea permet en outre de réduire 
 * le nombre de mesure car les mesures successives sont plus souvent identiques et donc ne sont pas écrites.
 */
class OwConfig extends OwConfigAbstract {
    /** longitude latitude pour sunset sunrise */
    public $location_sunrisesunset = array(
    	SunriseSunset::LOCATION_ST_SEBASTIEN_SUR_LOIRE_LATITUDE,
    	SunriseSunset::LOCATION_ST_SEBASTIEN_SUR_LOIRE_LONGITUDE
    );

    /**
     * Déclaration des sondes et des éléments à surcharger par rapport à la classe Abstraites
     */
    public $sensors = array(
        '10.3FC993020800' => array(
            array(
                'alias' => 'Indoor temperature',
                'type' => iOwSensor::TYPE_TEMPERATURE10,
                'display' => array(
                    //OwSensorAbstract::DISPLAY_WIDGET,
                    //OwSensorAbstract::DISPLAY_GRAPH_SECONDE,
                    //OwSensorAbstract::DISPLAY_GRAPH_MINUTE,
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
                //'correction' => 'X + 0', // permet de réaliser des corrections sur la valeur de la mesure de la sonde
                'accuracy' => OwSensorAbstract::ACCURACY_DISPLAY, // OwSensorAbstract::ACCURACY_DISPLAY OwSensorAbstract::ACCURACY_ACCURATE par défaut c'est OwSensorAbstract::ACCURACY_DISPLAY.
            )
        ),
        '26.CE9E6E010000' => array(
/*
            array(
                'alias' => 'Temperature (lum)', // pourrie comme sonde, à ne pas utiliser !
                'type' => iOwSensor::TYPE_TEMPERATURE26,
                'correction' => -0, // permet de réaliser des corrections sur la valeur de la mesure de la sonde
                'display' => array(
		),
            ),
*/
            array(
                'alias' => 'Light',
                'type' => iOwSensor::TYPE_LUMINOSITE,
                'display' => array(
//                    OwSensorAbstract::DISPLAY_WIDGET,
//                    OwSensorAbstract::DISPLAY_GRAPH_SECONDE,
//                    OwSensorAbstract::DISPLAY_GRAPH_HEURE,
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
            )
        ),
        /*
        '26.FE9E6E010000' => array(
            array(
                'alias' => 'Outdoor temperature (hum)', // pourrie comme sonde, à ne pas utiliser !
                'type' => iOwSensor::TYPE_TEMPERATURE26,
                'display' => array(
                    //OwSensorAbstract::DISPLAY_WIDGET,
                    //OwSensorAbstract::DISPLAY_GRAPH_SECONDE,
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
                'correction' => 'X + 1.3', // permet de réaliser des corrections sur la valeur de la mesure de la sonde
            ),
            array(
                'alias' => 'Humidity',
                'type' => iOwSensor::TYPE_HUMIDITE,
                'display' => array(
                    //OwSensorAbstract::DISPLAY_WIDGET,
                    //OwSensorAbstract::DISPLAY_GRAPH_SECONDE,
                    //OwSensorAbstract::DISPLAY_GRAPH_MINUTE,
                    //OwSensorAbstract::DISPLAY_GRAPH_HEURE,
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
            )
        ),
        */
        '28.4AEE7A030000' => array( // sonde étanche    
            array(
                'alias' => 'Attic temperature',
                'type' => iOwSensor::TYPE_TEMPERATURE,
                'display' => array(
                    OwSensorAbstract::DISPLAY_WIDGET,
                    OwSensorAbstract::DISPLAY_GRAPH_SECONDE,
                    OwSensorAbstract::DISPLAY_GRAPH_MINUTE,
                    OwSensorAbstract::DISPLAY_GRAPH_HEURE,
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
            )
        ),
        '28.4DCB7A030000' => array( // sonde étanche    
            array(
                'alias' => 'Outdoor temperature',
                'type' => iOwSensor::TYPE_TEMPERATURE,
                'display' => array(
                    OwSensorAbstract::DISPLAY_WIDGET,
                    OwSensorAbstract::DISPLAY_GRAPH_SECONDE,
                    OwSensorAbstract::DISPLAY_GRAPH_MINUTE,
                    OwSensorAbstract::DISPLAY_GRAPH_HEURE,
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
            )
        ),
        '12.E8407D000000' => array(
            array(
                'alias' => 'Relais Minuteur',
                'type' => iOwSensor::TYPE_RELAIS,
                'display' => array(
                    OwSensorAbstract::DISPLAY_WIDGET,
                    OwSensorAbstract::DISPLAY_GRAPH_SECONDE,
                    OwSensorAbstract::DISPLAY_GRAPH_MINUTE,
                    OwSensorAbstract::DISPLAY_GRAPH_HEURE,
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
            )
        ),
/*
        'teleinfo' => array( // sonde teleinfo
            array(
                'alias' => 'Puissance consommée',
                'type' => iOwSensor::TYPE_TELEINFO_PUISSANCE,
                'display' => array(
                    OwSensorAbstract::DISPLAY_WIDGET
                ), // permet d'afficher ou non les mesures d'une sonde sur les différents graphiques et page d'accueil
                'accuracy' => OwSensorAbstract::ACCURACY_ACCURATE,
            )
        )
*/
    );
}

