<?php
/**
 * config du owserver ou client
 * 
 * éléments quasi générique de la classe OwConfig
 * Voir la classe OwConfig dans le fichier modele owconfig_modele.php pour déclarer vos sondes
 */
class OwConfigAbstract {
    /** adresse owserver */
    public $owserver_ip = '127.0.0.1'; //'raspberrypi.local';
    /** port de owserver */
    public $owserver_port = '4304';

    /** Fréquence des mesures de référence (toutes les sondes sont concernées) */
    public $daemon_mesure_frequence = 3; // une pause de X secondes entre chaque mesures

    /** fréquence d'analyse de la taille du fichier self::fichier_mesure_seconde */
    public $freq_analyse_file_size = 10000; // toutes les N mesures on regarde si le fichier self::fichier_mesure_seconde dépasse les self::max_line_fichier_mesure_seconde

    /** Séparateur de décimal */
    public $separateur_decimal = '.';

    /** répertoire de sortie pour les mesures */
    public $dir_output_mesure = '/var/log/howl';

    /** longitude latitude pour sunset sunrise */
    public $location_sunrisesunset = array(
    	SunriseSunset::LOCATION_ST_SEBASTIEN_SUR_LOIRE_LATITUDE,
    	SunriseSunset::LOCATION_ST_SEBASTIEN_SUR_LOIRE_LONGITUDE
    );

    /** Nom du fichier contenant les mesures par secondes */
    public $fichier_mesure_seconde = 'mesure_seconde.csv';
    /** Nombre max cible de mesure dans le fichier seconde (ce chiffre pourra être temporairement dépassé) */
    public $max_line_fichier_mesure_seconde = 500000;
    /** Nombre max de point sur le graph seconde */
    public $max_point_graph_seconde = 7000; // environ 2 jours
    /**
     * taille d'une ligne du fichier seconde
     * la taille de la ligne correspond au nombre de caractère de la ligne.
     * elle varie en fonction du nombre de sonde et de la précision des mesures.
     * Il faut indiquer la valeur nominale.
     */
    public $fichier_mesure_seconde_taille_ligne = null; //43; // 54; // 2014/03/05 07:43:01,14.5625,70.1,12.7062,93.7,12.6250

    /** Nom du fichier contenant les mesures par minutes */
    public $fichier_mesure_minute = 'mesure_minute.csv';
    /** Nombre max cible de mesure dans le fichier minute (ce chiffre pourra être temporairement dépassé) */
    public $max_line_fichier_mesure_minute = 500000;
    /** Nombre max de point sur le graph minute */
    public $max_point_graph_minute = 10000; // environ 15 jours
    /**
     * taille d'une ligne du fichier minute
     * Ce paramètre est désormais déterminé automatiquement
     */
    public $fichier_mesure_minute_taille_ligne = null;

    /** Nom du fichier contenant les mesures par heure */
    public $fichier_mesure_heure = 'mesure_heure.csv';
    /** Nombre max de point sur le graph heure */
    public $max_point_graph_heure = 10000; // environ 2 ans
    /**
     * taille d'une ligne du fichier heure
     * Ce paramètre est désormais déterminé automatiquement
     */
    public $fichier_mesure_heure_taille_ligne = null;

    /** Nom du fichier contenant les mesures min et max par heure */
    public $fichier_minmax_heure = 'mesure_minmax_heure.csv';
    /** Nom du fichier contenant les mesures min et max pour today */
    public $fichier_minmax_today = 'mesure_minmax_today.csv';
    /** Nom du fichier contenant les mesures min et max pour yesterday */
    public $fichier_minmax_yesterday = 'mesure_minmax_yesterday.csv';
    /** Nom du fichier contenant les sunset sunrise */
    public $fichier_sunrisesunset = 'sunrise_sunset.json';

    /** Nom du fichier contenant les données de teleinfo */
    public $fichier_teleinfo_source = 'mesure_teleinfo_source.json';
    /** Nom du fichier contenant les valeurs de l'heure pecedente */
    public $fichier_teleinfo_heure_previous = 'mesure_teleinfo_heure_previous.json';
    /** Nom du fichier contenant les valeurs de la journee pecedente */
    public $fichier_teleinfo_journee_previous = 'mesure_teleinfo_journee_previous.json';

    /** format de la date */
    public $date_format = 'Y/m/d H:i:s'; //'Y-m-d H:i:s';

    
    /**
     * Initialisation de l'objet et principalement des "taille" des lignes.
     */
    public function __construct(){
        // taille initiale = date +\n
        $this->fichier_mesure_heure_taille_ligne = $this->fichier_mesure_minute_taille_ligne = $this->fichier_mesure_seconde_taille_ligne = 20;
        foreach ($this->sensors as $cur_sensor_id_conf => $sensors_conf) {
            foreach ($sensors_conf as $cur_sensor_conf) {
                if ($cur_sensor_conf['type'] === iOwSensor::TYPE_LUMINOSITE or 
                    $cur_sensor_conf['type'] === iOwSensor::TYPE_HUMIDITE or 
                    $cur_sensor_conf['type'] === iOwSensor::TYPE_RELAIS
                    ) {
                    if (isset($cur_sensor_conf['accuracy']) and $cur_sensor_conf['accuracy'] === OwSensorAbstract::ACCURACY_ACCURATE) {
                        $this->fichier_mesure_heure_taille_ligne += 15;//
                        $this->fichier_mesure_minute_taille_ligne += 5;//
                        $this->fichier_mesure_seconde_taille_ligne += 5;//
                    }else{
                        $this->fichier_mesure_heure_taille_ligne += 9;//
                        $this->fichier_mesure_minute_taille_ligne += 3;//
                        $this->fichier_mesure_seconde_taille_ligne += 3;//
                    }
                }else{
                    if (isset($cur_sensor_conf['accuracy']) and $cur_sensor_conf['accuracy'] === OwSensorAbstract::ACCURACY_ACCURATE) {
                        $this->fichier_mesure_heure_taille_ligne += 24;//
                        $this->fichier_mesure_minute_taille_ligne += 8;//
                        $this->fichier_mesure_seconde_taille_ligne += 8;//
                    }else{
                        $this->fichier_mesure_heure_taille_ligne += 15;//
                        $this->fichier_mesure_minute_taille_ligne += 5;//
                        $this->fichier_mesure_seconde_taille_ligne += 5;//
                    }
                }
            }
        }
    }

    /** retourne la date courante au format désiré */
    public function getTime($timestamp=null){
        if ($timestamp === null) {
            /*
            if ($this->date_format == 'microtime') {
                return round(microtime(true) * 1000);
            }
            */
            return date($this->date_format);
        }else{
            return date($this->date_format, $timestamp);
        }
    }
}

/** chargement du fihcier de config */
if (file_exists('/etc/howl_config_perso.php')) {
    require '/etc/howl_config_perso.php';
}else{
    require '/opt/howl/config/owconfig_modele.php';
}
