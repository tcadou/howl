<?php

interface iOwSensor{
    /** définintion des tyes de sondes */
    /** capteur de type 28 et les autres */
    const TYPE_TEMPERATURE = 'temperature-type28';
    /** capteur de type 10 */
    const TYPE_TEMPERATURE10 = 'temperature-type10';
    /** capteur de type 26 */
    const TYPE_TEMPERATURE26 = 'temperature-type26';
    const TYPE_HUMIDITE = 'humidite';
    const TYPE_LUMINOSITE = 'luminosite';
    const TYPE_RELAIS = 'relais';
    const TYPE_PLUIE = 'pluie';
    const TYPE_SON = 'son';
    const TYPE_TELEINFO_PUISSANCE = 'puissance';
    const TYPE_INCONNU = 'inconnu';

    /**
     * set une valeur sur le sensor
     *
     * Tous les path sont préfixés par l'identifiant du sensor (ex : /28.4AEE7A030000).
     */
    function set($path, $value='');

    /**
     * récupère la mesure du sensor associée au type de la sonde (fait appel à triggerMesure).
     * 
     */
    function getMesure($flag_changement_periode);

    /**
     * lance la requête de mesure à owserver
     */
    function triggerMesure($flag_changement_periode);

    /**
     * get l'ID' du sensor
     * @return string
     */
    function getId();

    /**
     * get l'ID' complet du sensor (inclu le type de sonde)
     * @return string
     */
    function getFullId();

    /**
     * get l'alias' du sensor
     * @return string
     */
    function getAlias();

    /**
     * get une valeur du sensor
     *
     * Tous les path sont préfixés par l'identifiant de la sensor (ex : /28.4AEE7A030000).
     */
    function get($path='/', $get_type=OWNET_MSG_READ, $return_full_info_array=false, $parse_php_type=true);
}
