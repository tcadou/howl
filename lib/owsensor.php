<?php

/**
 * iterateur de liste de sonde retournée par OwNet
 * Permet de formater les données retournées par OwNet
 */
class OwSensorOwNetIterator extends ArrayIterator {
    public function current (){
        $current = parent::current();
        $current['data_php'] = str_replace('/', '', $current['data_php']);
        return $current;
    }
}

/** iterateur de sondes */
class OwSensorFilter extends FilterIterator {
    public function __construct(Iterator $iterator) {
        parent::__construct($iterator);
    }
    public function accept() {
        $match = $this->getInnerIterator()->current();
        if( preg_match('/^..\..*/', $match['data_php']) === 1 and strlen($match['data_php']) === 15) {
            return true;
        }
        return false;
    }
}

/**
 * Gestion d'un sensor
 *
 * décrit un sensor et permet d'interroger/configurer ce sensor au travers du owNet sur owserver
 */
abstract class OwSensorAbstract {

    /** instance de OWNet ou OwTeleinfo */
    protected $owMesureHandler = null;
    
    /** id du sensor */
    protected $id = null;
    
    /** alias du sensor */
    protected $alias = null;
    
    /** type du sensor */
    protected $type = null;

    /** path de la valeur de la mesure */
    protected $mesure_path = null;

    /** constante pour la précision */
    const ACCURACY_ACCURATE = 'ACCURATE';
    const ACCURACY_DISPLAY = 'DISPLAY';

    /** définintion des différents affichages possibles */
    /** affichage partout */
    const DISPLAY_EVERYWHERE = 'display_everywhere';
    /** affichage en tant que widget */
    const DISPLAY_WIDGET = 'display_widget';
    /** affichage sur le graph seconde */
    const DISPLAY_GRAPH_SECONDE = 'display_graph_seconde';
    /** affichage sur le graph minute */
    const DISPLAY_GRAPH_MINUTE = 'display_graph_minute';
    /** affichage sur le graph heure */
    const DISPLAY_GRAPH_HEURE = 'display_graph_heure';
    /** affichage sur le graph teleinfo */
    const DISPLAY_GRAPH_TELEINFO = 'display_graph_teleinfo';

    /**
     * valeur de la mesure du capteur courante
     * voir $data pour le reste des infos sur le capteur
     * Toutes les autres données du capteur sont fournies par la méthode __get et __set afin de centraliser les données
     */
    public $mesure = 'NO_MESURE';
    
    /**
     * données en vrac associée au capteur
     * Toutes les autres données du capteur sont fournies par la méthode __get et __set afin de centraliser les données
     *
     * On stock entre autre :
     * - mesure 17.3750
     * - mesure_today_min 16.3750
     * - mesure_today_min_date 2014/01/30 04:26:09
     * - mesure_today_max 17.3750
     * - mesure_today_max_date 2014/01/30 12:18:54
     * - mesure_yesterday_min 10.6250
     * - mesure_yesterday_min_date 2014/01/29 08:37:04
     * - mesure_yesterday_max 17.7500
     * - mesure_yesterday_max_date 2014/01/29 19:41:04
     * - mesure_trend 1 => pour la mesure courante (mesure)
     * - mesure_yesterday 14.5000
     * - mesure_lastweek 09.0625
     */
    protected $data = array();
    
    /** unité de la mesure du capteur */
    protected $unite = '';
    
    /** unité complète de la mesure du capteur */
    protected $unite_full = '';
    
    /** état du capteur, actif (true), inactif (false) ou état inconu (null) */
    protected $actif = null;
    
    /** état du capteur, actif (true), inactif (false) ou état inconu (null) */
    protected $decimal_separator = '.';

    /**
     * creation du sensor
     * cela revient a fournir les paramètres présents dans le fichier de config au constructeur.
     * 
     * @param OwNet|OwTeleinfo|null $owMesureHandler
     * @param string $sensor_id identifiant du capteur sous la forme "10.3FC993020800"
     * @param string $sensor_type type du capteur ; valeur de 'type' dans le fichier de config (cf iOwSensor::TYPE_...)
     * @param string $sensor_alias valeur de 'alias' dans le fichier de config
     * @param char $decimal_separator "." ou "," (cf OwConfig::separateur_decimal)
     * @param string $accuracy=self::ACCURACY_DISPLAY précision utilisée dans les fichiers CSV ; valeur de 'accuracy' dans le fichier de config (OwSensorAbstract::ACCURACY_...)
     * @param array|string $display affichage a réaliser ; valeur de 'display' dans le fichier de config (cf OwSensorAbstract::DISPLAY_...) 
     * ou OwSensorAbstract::DISPLAY_EVERYWHERE dans le cas ou on souhaite l'afficher partout
     * @param string $correction correction à appliquer à la mesure de la sonde
     */
    public function __construct(
            $owMesureHandler=null, 
            $sensor_id=null, 
            $sensor_type=null, 
            $sensor_alias=null, 
            $decimal_separator='.', 
            $accuracy=self::ACCURACY_DISPLAY, 
            $display=self::DISPLAY_EVERYWHERE,
            $correction=null
        ){
        if (! $owMesureHandler instanceof OWNet and ! $owMesureHandler instanceof OwTeleinfo and $owMesureHandler !== null) {
            throw new InvalidArgumentException('$owMesureHandler must be an instance of OwNet, OwTeleinfo or null');
        }
        $this->owMesureHandler = $owMesureHandler;
        $this->id = $sensor_id;
        $this->type = $sensor_type;
        $this->alias = $sensor_alias;
        $this->correction = $correction;

        if ($decimal_separator === null) {
            $decimal_separator = '.';
        }
        $this->decimal_separator = $decimal_separator;

        if ($accuracy === null) {
            $accuracy = self::ACCURACY_DISPLAY;
        }
        $this->accuracy = $accuracy;

        if ($display === null or $display === self::DISPLAY_EVERYWHERE) {
            $this->display = array(self::DISPLAY_EVERYWHERE);
        }else{
            $this->display = $display;    
        }
    }

    public function __toString(){
        $data = '';
        foreach ($this->data as $key => $value) {
            $data .= "\n\t".$key." ".$value;
        }
        $mesure = $this->getMesure();
        if (is_array($mesure)) {
            foreach ($mesure as $key => $value) {
                $s_mesure .= "$value ($key), ";
            }
        }else{
            $s_mesure = $mesure;
        }
        return $this->getAlias()." (".$this->getFullId().")".
        "\n\tmesure $s_mesure".
        $data;
    }

    /**
     * - mesure 17.3750
     * - mesure_today_min 16.3750
     * - mesure_today_min_date 2014/01/30 04:26:09
     * - mesure_today_max 17.3750
     * - mesure_today_max_date 2014/01/30 12:18:54
     * - mesure_yesterday_min 10.6250
     * - mesure_yesterday_min_date 2014/01/29 08:37:04
     * - mesure_yesterday_max 17.7500
     * - mesure_yesterday_max_date 2014/01/29 19:41:04
     * - mesure_trend 1 => pour la mesure courante (mesure)
     * - mesure_yesterday 14.5000
     * - mesure_lastweek 09.0625
     */
    public function getHtmlWidget(){
        if ($this->mesure_trend === 1) {
            $trend = 'increase';
        }elseif ($this->mesure_trend === -1) {
            $trend = 'decrease';
        }else{
            $trend = 'stable';
        }

        $logo_mesure = 'sensor_unknown';
        $mesure = $this->mesure;
        if (is_array($this->mesure)) {
            // dans le cas de la téléinfo, la mesure contient plusieurs valeur en fonction de la période
            $mesure = $this->mesure[BatchMesure::CHANGEMENT_SECONDE];
        }
        switch ($this->getType()) {
            case iOwSensor::TYPE_TEMPERATURE:
            case iOwSensor::TYPE_TEMPERATURE10:
            case iOwSensor::TYPE_TEMPERATURE26:
                if ($mesure > 23) {
                    $logo_mesure = 'tempeture_warm';
                }elseif ($mesure < 14) {
                    $logo_mesure = 'tempeture_cold';
                }else{
                    $logo_mesure = 'tempeture_confortable';
                }
                break;
            case iOwSensor::TYPE_LUMINOSITE:
                if ($mesure > 60) {
                    $logo_mesure = 'light_day';
                }elseif ($mesure < 40) {
                    $logo_mesure = 'light_night';
                }else{
                    $logo_mesure = 'light_dark';
                }
                break;
            case iOwSensor::TYPE_HUMIDITE:
                if ($mesure > 70) {
                    $logo_mesure = 'humidity_wet';
                }elseif ($mesure < 50) {
                    $logo_mesure = 'humidity_dry';
                }else{
                    $logo_mesure = 'humidity_humide';
                }
                break;
            case iOwSensor::TYPE_RELAIS:
                if ($mesure > 0) {
                    $logo_mesure = 'relay_open';
                }else{
                    $logo_mesure = 'relay_close';
                }
                break;
            case iOwSensor::TYPE_TELEINFO_PUISSANCE:
                $logo_mesure = 'teleinfo';
                break;
            case iOwSensor::TYPE_INCONNU:
                $logo_mesure = 'sensor_unknown';
                break;
        }


        $widget = '
        <div class="box">
            <div class="box_titre1">
                <div class="titre empile">'.$this->getAlias().'</div>
            </div>
            <div class="box_body">
                <div class="block today_mesure">
                    <div class="empile mesure current_mesure">
                        <div class="empile logo_mesure '.$logo_mesure.'">&nbsp;</div>
                        <div class="empile value">'.$this->formatDisplayMesure($mesure).'</div>
                        <div class="empile unit">'.(($this->getType()==iOwSensor::TYPE_LUMINOSITE or $this->getType()==iOwSensor::TYPE_HUMIDITE) ? $this->getUnite() : $this->getUniteFull()).'</div>
                        <div class="empile logo_trend '.$trend.'">&nbsp;</div>
                    </div>
                    <div class="empile mesure_minmax today_minmax">
                        <div class="max">
                            <div class="empile value" title="Max today">'.$this->formatDisplayMesure($this->mesure_today_max).'</div>
                            <div class="empile unit" title="Max today">'.$this->getUnite().'</div>
                            <div class="empile hour" title="Hour of max today">('.(($this->mesure_today_max_date === null) ? '∅' : substr($this->mesure_today_max_date, 11, 5)).')</div>
                        </div>
                        <div class="min">
                            <div class="empile value" title="Min today">'.$this->formatDisplayMesure($this->mesure_today_min).'</div>
                            <div class="empile unit" title="Min today">'.$this->getUnite().'</div>
                            <div class="empile hour" title="Hour of min today">('.(($this->mesure_today_min_date === null) ? '∅' : substr($this->mesure_today_min_date, 11, 5)).')</div>
                        </div>
                    </div>
                </div>
                <div class="block separation_h">
                    <div class="empile">&nbsp;</div>
                </div>
                <div class="block old_mesure">
                    <div class="empile yesterday_mesure">
                        <div class="soustitre">Day-1</div>
                        <div class="empile yesterday_block">
                            <div class="empile value">'.$this->formatDisplayMesure($this->mesure_yesterday).'</div>
                            <div class="empile unit">'.(($this->getType()==iOwSensor::TYPE_LUMINOSITE or $this->getType()==iOwSensor::TYPE_HUMIDITE) ? $this->getUnite() : $this->getUniteFull()).'</div>
                            <div class="empile mesure_minmax">
                                <div class="max">
                                    <div class="empile value" title="Max yesterday">'.$this->formatDisplayMesure($this->mesure_yesterday_max).'</div>
                                    <div class="empile unit" title="Max yesterday">'.$this->getUnite().'</div>
                                    <div class="empile hour" title="Hour of max yesterday">('.(($this->mesure_yesterday_max_date === null) ? '∅' : substr($this->mesure_yesterday_max_date, 11, 5)).')</div>
                                </div>
                                <div class="min">
                                    <div class="empile value" title="Min yesterday">'.$this->formatDisplayMesure($this->mesure_yesterday_min).'</div>
                                    <div class="empile unit" title="Min yesterday">'.$this->getUnite().'</div>
                                    <div class="empile hour" title="Hour of min yesterday">('.(($this->mesure_yesterday_min_date === null) ? '∅' : substr($this->mesure_yesterday_min_date, 11, 5)).')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="empile block separation_v">
                        <div class="empile">&nbsp;</div>
                    </div>
                    <div class="empile lastweek_mesure">
                        <div class="soustitre">Day-7</div>
                        <div class="empile">
                            <div class="mesure">
                                <div class="empile value">'.$this->formatDisplayMesure($this->mesure_lastweek).'</div>
                                <div class="empile unit">'.(($this->getType()==iOwSensor::TYPE_LUMINOSITE or $this->getType()==iOwSensor::TYPE_HUMIDITE) ? $this->getUnite() : $this->getUniteFull()).'</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';

        return $widget;
    }

    public function __set($name, $value){
        $this->data[$name] = $value;
    }

    public function __get($name){
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return null;
    }
    
    /**
     * set une valeur sur le sensor
     *
     * Tous les path sont préfixés par l'identifiant du sensor (ex : /28.4AEE7A030000).
     */
    public function set($path, $value=''){
        if ($this->owMesureHandler instanceof OwNet) {
            return $this->owMesureHandler->set("/".$this->id.$path, $value);
        }elseif ($this->owMesureHandler instanceof OwTeleinfo) {
            throw new BadMethodCallException("OwTeleinfo is a readonly sensor");
        }
        
        /*
        // test si la connexion au server a pu être établie
        $last_error = error_get_last();
        if(!empty($last_error) and $last_error['type'] == 1024 and strpos($last_error['message'], "Can't connect") !== false){
            $this->actif = false;
        }
        */
    }

    /**
     * get une valeur du sensor
     *
     * Tous les path sont préfixés par l'identifiant de la sensor (ex : /28.4AEE7A030000).
     */
    public function get($path='/', $get_type=OWNET_MSG_READ, $return_full_info_array=false, $parse_php_type=true){
        if ($this->owMesureHandler instanceof OwNet) {
            return $this->owMesureHandler->get("/".$this->id.$path, $get_type, $return_full_info_array, $parse_php_type);
        }elseif ($this->owMesureHandler instanceof OwTeleinfo) {
            return $this->owMesureHandler->get($path); // $path == clé du json de teleinfo ou la valeur de la puissnce instantanée ou consommée sur l'heure
        }
        /*
        if(!empty($last_error) and $last_error['type'] == 1024 and strpos($last_error['message'], "Can't connect") !== false){
            $this->actif = false;
        }
        */
    }

    /**
     * récupère la mesure et vérifie si la valeur retournée par owserver est valide.
     * Cette méthode permet de gérer de manière générique les problèmes de connexion a owserver
     * Dans le cas ou on ne souhaite pas réaliser ces vérifications, on peut appleler directement la méthode self:::get()
     * @return mixed La valeur en cas de succès sinon false
     */
    protected function getAndCheck($path){
        $mesure = $this->get($path);
        if ($mesure == 0) {
            /**//* @todo lorsqu'une sonde n'existe plus sur le réseaux, la méthode get() retourne NULL
            Il faut donc différencier les valeurs successive à "0" car  les sondes ne "répondent pas assez vite"
            et les sondes qui n'exsistent plus sur le réseaux.
            */
            /**/// On met en commentaire cette notice car elle survient à chaque boucle...
            /**//*trigger_error(
                "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est invalide.\n".
                    $path."=\"".$mesure."\" (pb de chauffe ?)\n".
                    "On interroge à nouveau la sonde car cette mesure n'est pas utilisée."
                , E_USER_NOTICE
            );
            */
            // deuxième tentative
            $mesure = $this->get($path);
            if (is_null($mesure)) {
                // La sonde ne retourne pas de valeur car elle est sans doute indisponible sur le réseau
                /*/trigger_error(
                    "La sonde ".$this->getFullId()." ne semble plus disponible sur le réseau.\n".
                    " La mesure retournée par OwNet/OwTeleinfo pour cette sonde est NULL.\n".
                        $path."=\"NULL\""
                    , E_USER_NOTICE
                );
                /**/
                return false;
            }elseif ($mesure === 0) {
                // @todo voir comment gérer ces problèmes de 0 de manière définitive.
                // pour le moment, on les ignore avec des valeur null
                /*/trigger_error(
                    "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est ENCORE invalide.\n".
                        $path."=\"".var_export($mesure, true)."\"\n".
                        "On remplace cette valeur par NULL."
                    , E_USER_NOTICE
                );
                /**///throw new OwSensorsUnexpectedValueException("La mesure retournée par OwNet est invalide ".$path."=\"".$mesure."\".");
                return false;
            } elseif ($mesure == 0) {
                // @todo voir comment gérer ces problèmes de 0 de manière définitive.
                // pour le moment, on les ignore avec des valeur null
                /*/trigger_error(
                    "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est ENCORE invalide.\n".
                        $path."=\"".var_export($mesure, true)."\"\n".
                        "On remplace cette valeur par NULL."
                    , E_USER_NOTICE
                );
                /**///throw new OwSensorsUnexpectedValueException("La mesure retournée par OwNet est invalide ".$path."=\"".$mesure."\".");
                return false;
            }
        }
        return $mesure;
    }

    /**
     * get le type du sensor
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * get la précision de la mesure
     * @return string OwSensorAbstract::ACCURACY_DISPLAY OwSensorAbstract::ACCURACY_ACCURATE
     */
    public function getAccuracy(){
        return $this->accuracy;
    }

    /**
     * get la correction de la sonde
     * @return string se trouve dans le fichier de config
     */
    public function getCorrection(){
        return $this->correction;
    }

    /**
     * get l'affichage à réaliser
     * @param string $display_exists 
     * @return array|boolean liste des affichages a réaliser. OwSensorAbstract::DISPLAY_... si $display_exists === null
     * sinon on retourne true ou false en fonction de l'affichage demandé. On test aussi la valeur OwSensorAbstract::DISPLAY_EVERYWHERE
     */
    public function getDisplay($display_exists=null){
        if ($display_exists === null) {
          return $this->display;
        }
        return in_array($display_exists, $this->display) or in_array(self::DISPLAY_EVERYWHERE, $this->display);
    }

    /**
     * get l'ID du sensor
     * @return string
     */
    public function getId(){
        return $this->id;
    }

    /**
     * get l'ID complet du sensor (inclu le type de sonde)
     * @return string
     */
    public function getFullId(){
        return $this->id.'_'.$this->type;
    }

    /**
     * get l'alias du sensor
     * @return string
     */
    public function getAlias(){
        return $this->alias;
    }

    /**
     * récupère la mesure du sensor
     * 2 cas possibles :
     * - récupération au travers de la méthode triggerMesure qui intérroge owserver
     * - retourne $this->mesure qui a été définie manuellement
     * @param BatchMesure::CHANGEMENT_SECONDE | BatchMesure::CHANGEMENT_JOURNEE | BatchMesure::CHANGEMENT_HEURE | BatchMesure::CHANGEMENT_MINUTE $flag_changement_periode détermine quelle période change.
     */
    public function getMesure($flag_changement_periode = BatchMesure::CHANGEMENT_SECONDE){
        if ($this->mesure !== 'NO_MESURE') {
            return $this->mesure;
        }
        try {
            $mesure = $this->triggerMesure($flag_changement_periode);
        } catch (Exception $e) {
            if( strpos($e->getMessage(), "Can't create") !== false or 
                strpos($e->getMessage(), "Can't connect") !== false or
                strpos($e->getMessage(), "Disconnected") !== false){
                throw new OwSensorsConnexionException($e->getMessage());
            }else{
                throw new OwnetErrorException($e->getMessage());
            }
        }
        return $mesure;
    }

    /**
     * retourne la mesure pour l'affichage
     */
    public function formatDisplayMesure($mesure){
        if ($mesure === 'NO_MESURE' or $mesure === null) {
            return '∅';
        }
        return round($mesure, 1);
    }

    /**
     * récupère l'unité de la mesure du sensor associée au type de la sonde
     */
    public function getUnite(){
        return $this->unite;
    }

    /**
     * récupère l'unité de la mesure du sensor associée au type de la sonde
     */
    public function getUniteFull(){
        return $this->unite_full;
    }

    /**
     * retouorne la valeur de la tendance à partir de 2 mesures
     *
     * Attention à gérer les valeurs NULL
     * 
     * Cette fonction est calibrée pour les températures, il faut la surchargée pour gérer d'autres types de mesure.
     * @return integer -1 | 0 | 1 (decrease | stable | increase )
     */
    public function getTrend($older_value, $newer_value){
        if (is_null($older_value) or is_null($newer_value)) {
            // si une des valeurs est null on dit que c'est stable
            return 0;
        }
        if (abs($older_value - $newer_value) < 0.1) {
            // stable
            return 0;
        }elseif ($older_value > $newer_value) {
            return -1;
        }else{
            return 1;
        }
    }

    /**
     * set le status du sensor
     */
//    public function setActif($actif){
//        $this->actif = $actif;
//    }

    /**
     * get le status du sensor
     * @return boolean
     */
//    public function getActif(){
//        return $this->actif;
//    }

}


/**
 * Gestion d'un sensor générique
 */
class OwSensor extends OwSensorAbstract implements iOwSensor {
    protected $type = iOwSensor::TYPE_INCONNU;
    public function __construct($owNet, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owNet, $sensor_id, iOwSensor::TYPE_INCONNU, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }
    public function triggerMesure($flag_changement_periode){
        trigger_error ("Le sensor '".$this->id."' est generique et ne dispose pas de mesure", E_USER_NOTICE);
    }
}

/**
 * Gestion d'un sensor de temperature
 */
class OwSensorTemperature extends OwSensorAbstract implements iOwSensor {
    protected $unite = '°';
    protected $unite_full = '°C';
    public function __construct($owNet, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owNet, $sensor_id, iOwSensor::TYPE_TEMPERATURE, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }
    public function triggerMesure($flag_changement_periode){
        $mesure = $this->getAndCheck('/temperature');
        if ($mesure === false) {
            return null;
        }
        if (!is_null($this->correction)) {
            eval("\$mesure=".str_replace("X", $mesure, $this->correction).";");
            //$mesure = $mesure + $this->correction;
        }
        if ($mesure > 60 or $mesure < -40) {
            // Les mesures semblent fausses... on remplace par NULL
            trigger_error(
                "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est invalide.\n".
                    $this->mesure_path."= -40 < \"".$mesure."\" < 60\n".
                    "On remplace cette valeur par NULL."
                , E_USER_NOTICE
            );
            return null;
        }
        if ($this->accuracy == self::ACCURACY_DISPLAY) {
            $decimal_number = 1;
        }else{
            $decimal_number = 4;
        }
        return str_pad(
            number_format(
                $mesure,
                $decimal_number,
                $this->decimal_separator,
                ''
            ),
            3 + $decimal_number, // 7 ou 4
            '0', 
            STR_PAD_LEFT
        );
    }
}

/**
 * Gestion d'un sensor de temperature de type 10
 */
class OwSensorTemperature10 extends OwSensorAbstract implements iOwSensor {
    protected $unite = '°';
    protected $unite_full = '°C';
    public function __construct($owNet, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owNet, $sensor_id, iOwSensor::TYPE_TEMPERATURE10, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }
    public function triggerMesure($flag_changement_periode){
        $mesure = $this->getAndCheck('/temperature');
        if ($mesure === false) {
            return null;
        }
        if (!is_null($this->correction)) {
            eval("\$mesure=".str_replace("X", $mesure, $this->correction).";");
            //$mesure = $mesure + $this->correction;
        }
        if ($mesure > 60 or $mesure < -40) {
            // Les mesures semblent fausses... on remplace par NULL
            trigger_error(
                "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est invalide.\n".
                    $this->mesure_path."= -40 < \"".$mesure."\" < 60\n".
                    "On remplace cette valeur par NULL."
                , E_USER_NOTICE
            );
            return null;
        }
        if ($this->accuracy == self::ACCURACY_DISPLAY) {
            $decimal_number = 1;
        }else{
            $decimal_number = 4;
        }
        return str_pad(
            number_format(
                $mesure,
                $decimal_number,
                $this->decimal_separator,
                ''
            ),
            3 + $decimal_number, // 7 ou 4
            '0', 
            STR_PAD_LEFT
        );
    }
}

/**
 * Gestion d'un sensor de temperature de type 26
 */
class OwSensorTemperature26 extends OwSensorAbstract implements iOwSensor {
    protected $unite = '°';
    protected $unite_full = '°C';
    public function __construct($owNet, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owNet, $sensor_id, iOwSensor::TYPE_TEMPERATURE26, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }
    public function triggerMesure($flag_changement_periode){
        $mesure = $this->getAndCheck('/temperature');
        if ($mesure === false) {
            return null;
        }
        if (!is_null($this->correction)) {
            eval("\$mesure=".str_replace("X", $mesure, $this->correction).";"); // correction car cette sonde est pourrie
        }
        if ($mesure > 60 or $mesure < -40) {
            // Les mesures semblent fausses... on remplace par NULL
            trigger_error(
                "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est invalide.\n".
                    $this->mesure_path."= -40 < \"".$mesure."\" < 60\n".
                    "On remplace cette valeur par NULL."
                , E_USER_NOTICE
            );
            return null;
        }
        if ($this->accuracy == self::ACCURACY_DISPLAY) {
            $decimal_number = 1;
        }else{
            $decimal_number = 4;
        }
        return str_pad(
            number_format(
                $mesure,
                $decimal_number,
                $this->decimal_separator,
                ''
            ),
            3 + $decimal_number, // 7 ou 4
            '0', 
            STR_PAD_LEFT
        );
    }
}

/**
 * Gestion d'un sensor d'humidite
 */
class OwSensorHumidite extends OwSensorAbstract implements iOwSensor {
    protected $unite = '%';
    protected $unite_full = '% hum.';
    public function __construct($owNet, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owNet, $sensor_id, iOwSensor::TYPE_HUMIDITE, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }
    public function triggerMesure($flag_changement_periode){
        $f_vad = $this->getAndCheck('/VAD');
        if ($f_vad === false) {
            return null;
        }
        $mesure = (float) $f_vad;
        $mesure = $mesure * 33.33;
        if (!is_null($this->correction)) {
            eval("\$mesure=".str_replace("X", $mesure, $this->correction).";");
            //$mesure = $mesure + $this->correction; // correction car cette sonde est pourrie
        }
        if ($mesure > 100 or $mesure < 0) {
            // Les mesures semblent fausses... on remplace par NULL
            trigger_error(
                "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est invalide.\n".
                    $this->mesure_path."= 0 < \"".$mesure."\" < 100\n".
                    "On remplace cette valeur par NULL."
                , E_USER_NOTICE
            );
            return null;
        }
        if ($this->accuracy == self::ACCURACY_DISPLAY) {
            $decimal_number = 0;
        }else{
            $decimal_number = 1;
        }
        return str_pad(
            number_format(
                $mesure,
                $decimal_number,
                $this->decimal_separator,
                ''
            ),
            3 + $decimal_number, // 4 ou 3
            '0', 
            STR_PAD_LEFT
        );
    }
    public function getTrend($older_value, $newer_value){
        if (is_null($older_value) or is_null($newer_value)) {
            // si une des valeurs est null on dit que c'est stable
            return 0;
        }
        if (abs($older_value - $newer_value) < 5) {
            // stable
            return 0;
        }elseif ($older_value > $newer_value) {
            return -1;
        }else{
            return 1;
        }
    }
}

/**
 * Gestion d'un sensor de luminosite
 */
class OwSensorLuminosite extends OwSensorAbstract implements iOwSensor {
    protected $unite = '%';
    protected $unite_full = '% light';
    public function __construct($owNet, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owNet, $sensor_id, iOwSensor::TYPE_LUMINOSITE, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }
    public function triggerMesure($flag_changement_periode){
        $f_vad = $this->getAndCheck('/VAD');
        if ($f_vad === false) {
            return null;
        }
        $f_vad = (float) $f_vad;

        $f_vdd = $this->getAndCheck('/VDD');
        if ($f_vdd === false) {
            return null;
        }
        $f_vdd = (float) $f_vdd;

        if ($f_vad > $f_vdd) {
            // VAD ne peux être supérieur à VDD sauf en cas de problème de référence de masse ???
            trigger_error(
                "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est invalide.\n".
                    $this->mesure_path."=\"".$f_vad."\" > /VDD=\"".$f_vdd."\" (pb de masse ?)\n".
                    "On remplace cette valeur par NULL."
                , E_USER_NOTICE
            );
            return null; // NULL (qui sera écrit comme "Nan" dans le CSV) sera interprété par un trou dans la courbe
        }
        $mesure = ($f_vad * 100) / $f_vdd;
        if (!is_null($this->correction)) {
            eval("\$mesure=".str_replace("X", $mesure, $this->correction).";");
            //$mesure = $mesure + $this->correction; // correction car cette sonde est pourrie
        }
        if ($mesure > 100 or $mesure < 0) {
            // Les mesures semblent fausses... on remplace par NULL
            trigger_error(
                "La mesure retournée par OwNet pour la sonde ".$this->getFullId()." est invalide.\n".
                    $this->mesure_path."= 0 < \"".$mesure."\" < 100\n".
                    "On remplace cette valeur par NULL."
                , E_USER_NOTICE
            );
            return null;
        }
        if ($this->accuracy == self::ACCURACY_DISPLAY) {
            $decimal_number = 0;
        }else{
            $decimal_number = 1;
        }
        return str_pad(
            number_format(
                $mesure,
                $decimal_number,
                $this->decimal_separator,
                ''
            ),
            3 + $decimal_number, // 4 ou 3
            '0', 
            STR_PAD_LEFT
        );
    }
    public function getTrend($older_value, $newer_value){
        if (is_null($older_value) or is_null($newer_value)) {
            // si une des valeurs est null on dit que c'est stable
            return 0;
        }
        if (abs($older_value - $newer_value) < 5) {
            // stable
            return 0;
        }elseif ($older_value > $newer_value) {
            return -1;
        }else{
            return 1;
        }
    }
}

/**
 * Gestion d'un sensor d'entrée sortie (relais)
 */
class OwSensorRelais extends OwSensorAbstract implements iOwSensor {
    protected $unite = '';
    protected $unite_full = '';
    public function __construct($owNet, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owNet, $sensor_id, iOwSensor::TYPE_RELAIS, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }
    /** ouvre le relais => éteind l'appareil branché */
    public function open(){
        $this->set("/PIO.A", "0");
    }
    /** ferme le relais => allume l'appareil branché */
    public function close(){
        $this->set("/PIO.A", "1");
    }
    /** ferme le relais => allume l'appareil branché */
    public function toggle(){
        if($this->getMesure() === 1){
            $this->open();
        }else{
            $this->close();
        }
    }
    /**
     * @return integer 100 (au lieu de 1) pour le statut fermé, 0 pour ouvert
     * Ceci afin de pouvoir tracer les mesures dans les graphs.
     * Dans le cas de l'affichage on convertit à nouveau
     */
    public function triggerMesure($flag_changement_periode){
        // pas de contrôle possible de la valeur du sensor car la valeur 0 correspond à un status valide.
        $ret_mesure = (integer) $this->get("/PIO.A");
        if ($ret_mesure === 1) {
            return 100;
        }
        return 0;
    }
    public function getTrend($older_value, $newer_value){
        if (is_null($older_value) or is_null($newer_value)) {
            // si une des valeurs est null on dit que c'est stable
            return 0;
        }
        if ($older_value == $newer_value) {
            // stable
            return 0;
        }elseif ($older_value > $newer_value) {
            return -1;
        }else{
            return 1;
        }
    }
    /**
     * retourne la mesure pour l'affichage
     */
    public function formatDisplayMesure($mesure){
        if ($mesure === 'NO_MESURE' or $mesure === null) {
            return '∅';
        }
        if ($mesure == 100) {
            return 'on';
        }
        return 'off';
    }

}


/**
 * Gestion d'un sensor de type teleinfo pour la puissance instantanée
 */
class OwSensorTeleinfo extends OwSensorAbstract implements iOwSensor {
    protected $unite = 'va';
    protected $unite_full = 'VA';
    public function __construct($owTeleinfo, $sensor_id=null, $sensor_alias=null, $decimal_separator=null, $accuracy=null, $display=null, $correction=null){
        parent::__construct($owTeleinfo, $sensor_id, iOwSensor::TYPE_TELEINFO_PUISSANCE, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
    }

    /**
     * @return mesure ATTENTION on retourne un tableau de mesure
     */
    public function triggerMesure($flag_changement_periode){
        $a_mesure = $this->getAndCheck($flag_changement_periode);
        $output = array();

        if ($a_mesure === false) {
            // on créé un tableau vide pour le parcourir ensuite
            switch ($flag_changement_periode) {
                case BatchMesure::CHANGEMENT_SECONDE:
                    $a_mesure = array_fill (0, 1, null);
                    break;
                case BatchMesure::CHANGEMENT_MINUTE:
                    $a_mesure = array_fill (0, 2, null);
                    break;
                case BatchMesure::CHANGEMENT_HEURE:
                    $a_mesure = array_fill (0, 3, null);
                    break;
                case BatchMesure::CHANGEMENT_JOURNEE:
                    $a_mesure = array_fill (0, 4, null);
                    break;
            }
        }
        foreach ($a_mesure as $key => $value) {
            if ($value === null) {
                $output[$key] = null;
                continue;
            }
            $mesure = (float) $value;
            if (!is_null($this->correction)) {
                eval("\$mesure=".str_replace("X", $mesure, $this->correction).";");
                //$mesure = $mesure + $this->correction; // correction car cette sonde est pourrie
            }
            if ($mesure < 0) {
                // Les mesures semblent fausses... on remplace par NULL
                trigger_error(
                    "La mesure retournée par OwTeleinfo pour la sonde ".$this->getFullId()." est invalide.\n".
                        $this->mesure_path."= 0 < \"".$mesure."\"\n".
                        "On remplace cette valeur par NULL."
                    , E_USER_NOTICE
                );
                $output[$key] = null;
                continue;
            }
            if ($this->accuracy == self::ACCURACY_DISPLAY) {
                $decimal_number = 0;
            }else{
                $decimal_number = 1;
            }
            $output[$key] = str_pad(
                number_format(
                    $mesure,
                    $decimal_number,
                    $this->decimal_separator,
                    ''
                ),
                5 + $decimal_number, // @todo définir le nombre de chiffre de la puissance du compteur
                '0', 
                STR_PAD_LEFT
            );
        }
/*
        if (count($output) === 1) {
            // si 1 seule valeur, alors on envoie pas sous forme de tableau
            return array_shift($output);
        }
*/
        return $output;
    }
    public function getTrend($older_value, $newer_value){
        if (is_null($older_value) or is_null($newer_value)) {
            // si une des valeurs est null on dit que c'est stable
            return 0;
        }
        if ($older_value > $newer_value) {
            return -1;
        }else{
            return 1;
        }
    }
    /**
     * retourne la mesure pour l'affichage
     */
    public function formatDisplayMesure($mesure){
        if ($mesure === 'NO_MESURE' or $mesure === null) {
            return '∅';
        }
        if ($this->accuracy == self::ACCURACY_DISPLAY) {
            $decimal_number = 0;
        }else{
            $decimal_number = 4; /**/// faire un premier relevé de compteur...
        }
        return number_format(
            ($mesure / 1000),
            $decimal_number,
            $this->decimal_separator,
            ' '
        );
    }
}


/**
 * Sensor Factory
 */
class OwSensorFactory {
    /**
     * creation du sensor
     * cela revient a fournir les paramètres présents dans le fichier de config
     * 
     * @param OwConfig|stdClass $owConfig config du server OW
     * @param 'sensors|csv' $input défini si les données sont récupérées de owserver/teleinfo ou des fichiers csv ou des deux.
     * @param string $sensor_id identifiant du capteur sous la forme "10.3FC993020800"
     * @param string $sensor_type type du capteur ; valeur de 'type' dans le fichier de config (cf iOwSensor::TYPE_...)
     * @param string $sensor_alias valeur de 'alias' dans le fichier de config
     * @param char $decimal_separator "." ou "," (cf OwConfig::separateur_decimal)
     * @param string $accuracy=self::ACCURACY_DISPLAY précision utilisée dans les fichiers CSV ; valeur de 'accuracy' dans le fichier de config (OwSensorAbstract::ACCURACY_...)
     * @param array|string $display affichage a réaliser ; valeur de 'display' dans le fichier de config (cf OwSensorAbstract::DISPLAY_...) 
     * ou OwSensorAbstract::DISPLAY_EVERYWHERE dans le cas ou on souhaite l'afficher partout
     * @param string $correction correction à appliquer à la mesure de la sonde
     */
    public static function getSensor(
            $owConfig,
            $input='sensors',
            $sensor_id=null, 
            $sensor_type=null, 
            $sensor_alias=null, 
            $decimal_separator=null, 
            $accuracy=null, 
            $display=null,
            $correction=null
        ){
        static $o_owNet = null;
        static $o_owTeleinfo = null;

        if ($input === 'sensors') {
            // récupération des données directement à partir des sondes
            if (is_null($o_owNet)) {
                $o_owNet = new owNet("tcp://".$owConfig->owserver_ip.":".$owConfig->owserver_port);
            }
            $cur_owNet = $o_owNet;
        }else{
            // on ne récupère pas encore les données en mode CSV car on récupère les données
            // de toutes les sondes en une fois.
            $cur_owNet = null;
            $cur_owTeleinfo = null;
        }
        switch ($sensor_type) {
            case iOwSensor::TYPE_LUMINOSITE :
                return new OwSensorLuminosite($cur_owNet, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
            case iOwSensor::TYPE_HUMIDITE :
                return new OwSensorHumidite($cur_owNet, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
            case iOwSensor::TYPE_TEMPERATURE :
                return new OwSensorTemperature($cur_owNet, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
            case iOwSensor::TYPE_TEMPERATURE10 :
                return new OwSensorTemperature10($cur_owNet, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
            case iOwSensor::TYPE_TEMPERATURE26 :
                return new OwSensorTemperature26($cur_owNet, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
            case iOwSensor::TYPE_RELAIS :
                return new OwSensorRelais($cur_owNet, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
            case iOwSensor::TYPE_TELEINFO_PUISSANCE :
                if (is_null($o_owTeleinfo)) {
                    $o_owTeleinfo = new OwTeleinfo($owConfig);
                }
                $cur_owTeleinfo = $o_owTeleinfo;
                return new OwSensorTeleinfo($cur_owTeleinfo, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
            case iOwSensor::TYPE_INCONNU :
            default:
                return new OwSensor($cur_owNet, $sensor_id, $sensor_alias, $decimal_separator, $accuracy, $display, $correction);
                break;
        }
    }
}
