<?php
/**
 * autoload, date, ...
 */
if (!isset($block_ownet_check) or $block_ownet_check !== true) {
    if(!file_exists('/usr/share/php/OWNet/ownet.php')){
        throw new OwnetLibNotFoundException("Vous devez installer OwNet (apt-get install libownet-php)");
    }
    /** ownet, lib de connexion au serveur owserver en PHP */
    require '/usr/share/php/OWNet/ownet.php';
}

// Fichier de log par défaut de l'application
define('HOWL_LOGFILE', '/var/log/howl/howl.log');

/** Exception d'entrée sortie */
class IOException extends UnderflowException {}
class CantWriteFileException extends IOException {}

/** lib principale */
require '/opt/howl/config/owconfig.php';

/** Chargement des autres classes */
function autoload_perso ($class) {
    if($class != 'OwSensors' and strpos($class, 'OwSensor') === 0 or strpos($class, 'Ownet') === 0){
        require_once '/opt/howl/lib/owsensor.php';
    }else{
        require_once '/opt/howl/lib/'.strtolower($class).'.php';
    }
}
spl_autoload_register('autoload_perso');

/** Exception englobant (à part quelques exceptions) toutes les erreurs de la classe OwNet */
class OwnetErrorException extends ErrorException {}

/** Gestionnaire d'erreur */
function error_handler_perso ($errno, $errstr, $errfile, $errline ) {
    if(strpos($errstr, 'Only variables should be passed by reference') !== false){
        return true;
    }
    if (basename($errfile) == 'ownet.php') {
        switch ($errno) {
            case E_WARNING:
            case E_NOTICE:
                return true;
            case E_USER_WARNING:
            case E_USER_NOTICE:
            case E_RECOVERABLE_ERROR:
            case E_USER_ERROR:
            default:
                throw new OwnetErrorException($errstr, $errno, 0, $errfile, $errline);
        }
    }
    switch ($errno) {
        case E_NOTICE:
            $level = 'E_NOTICE';
        case E_USER_NOTICE:
            $level = 'E_USER_NOTICE';
            Util::log(HOWL_LOGFILE, $level." : ".$errstr, $errfile, $errline, __FUNCTION__, 1);
            break;
        case E_WARNING:
            $level = 'E_WARNING';
        case E_USER_WARNING:
            $level = 'E_USER_WARNING';
        case E_RECOVERABLE_ERROR:
            $level = 'E_RECOVERABLE_ERROR';
        case E_USER_ERROR:
            $level = 'E_USER_ERROR';
        default:
            echo Util::log(HOWL_LOGFILE, $errstr, $errfile, $errline, __FUNCTION__, 2);
            throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
    return true;
}
set_error_handler('error_handler_perso');

function exception_handler_perso($e) {
    Util::log(HOWL_LOGFILE, $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString(), 2);
}

set_exception_handler('exception_handler_perso');
