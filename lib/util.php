<?php
/**
 * Un peu de bazarre !
 */
class Util{

	/** différents niveaux des log */
	const LOG_INFO = 0;
	/** différents niveaux des log */
	const LOG_IMPORTANT = 1;
	/** différents niveaux des log */
	const LOG_CRITICAL = 2;

	/**
	 * Écrit les messages de log
	 * @param integer $level 0 = info, 1 = normal (default), 2 = critique
	 */
	public static function log($log_file, $message, $file='?', $line='?', $method='', $level=1){
		if ($level === self::LOG_CRITICAL) {
			$level = '☢'; // ⛔
		}elseif ($level === self::LOG_IMPORTANT) {
			$level = '★';
		}elseif ($level === self::LOG_INFO) {
			$level = '⚑';
		}else{
			$level = '?';
		}
		$msg = date("Y/m/d H:i:s")." $level $level\n".
					"\t⚠ [".str_replace("\n", "\n\t  ", basename($file).":".trim($line." ".$method))."]\n".
					"\t▶ ".str_replace("\n", "\n\t  ", $message)."\n";
		file_put_contents($log_file, $msg, FILE_APPEND);
		return $msg;
	}
}