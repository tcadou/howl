<?php
/**
 * lib maison pour échanger avec owserver et gérer les sensors
 */

/** Classe mère gérant l'ensemble des Exception OwSensors et OwNet */
class OwSensorsException extends RuntimeException {}

/** Exception signifiant un problème de connexion à OwSensors */
class OwSensorsConnexionException extends OwSensorsException {}

/** Exception ... */
class OwSensorsSensorInitException extends OwSensorsException {}

/** Classe gérant les Exception OwNet */
class OwNetException extends OwSensorsException {}

class OwSensorsUnexpectedValueException extends UnexpectedValueException {}

class OwnetLibNotFoundException extends OwNetException {}

/**
 * gestion du owServer
 */
class OwSensors implements IteratorAggregate, Countable {

    /** mode debug */
    public $debug = false;
    
    /** fichier de log */
    protected $log_file = null;

    /** instance de owConfig */
    protected $owConfig = null;

    /** instances des sensors */
    public static $ow_sensors = null;
    
    /**
     * @param OwConfig|stdClass $owConfig config du server OW
     * @param 'sensors|csv' $input défini si les données sont récupérées de owserver/teleinfo ou des fichiers csv ou des deux.
     * C'est 'sensors' par défaut.
     */
    public function __construct($owConfig, $input='sensors', $log_file=null, $debug = false){
        if(! $owConfig instanceof OwConfig and ! $owConfig instanceof stdClass){
            throw new InvalidArgumentException('Type du paramètre $owConfig incorrect');
        }
        $this->owConfig = $owConfig;
        $this->debug = $debug;
        $this->log_file = $log_file;
        $this->input = $input;
        $this->loadSensors();
        if ($this->checkSensors() === false) {
            // @todo vide ???? je voulais faire quoi ici ???
        }
    }

    /** Iterateur pour faire foreach(OwSensors as OwSensor) */
    public function getIterator() {
        return new ArrayIterator((array)self::$ow_sensors);
    }

    /** count pour faire nb_OwSensor = count(OwSensors) */
    public function count(){
        return count((array)self::$ow_sensors);
    }

    /**
     * Charges les sensors en mémoire à partir du fichier de config
     *
     * @param 'sensors|csv' $input défini si les données sont récupérées de owserver/teleinfo ou des fichiers csv.
     * C'est 'sensors' par défaut.
     * @return integer le nombre de sensor. En cas d'erreur on retourne false
     */
    protected function loadSensors($input='sensors'){
        $this::$ow_sensors = array();

        // chargement des données à partir des fichiers CSV
        if ($this->input === 'csv') {
            // les mesures et les tendances
            $this->mesureCsv = new MesureCsv($this->owConfig);
            $this->mesureCsv->loadMesures();
            // la valeur min/max sur aujourd'hui/hier
            $minMax = new MinMax(
                $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_minmax_heure,
                $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_minmax_today,
                $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_minmax_yesterday
            );
            $debug_message = $minMax->getDebugMessage();
            if (count($debug_message) > 0 and !empty($this->log_file)) {
                Util::log($this->log_file, implode("\n", $debug_message), __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
            }
        }

        foreach ($this->owConfig->sensors as $cur_sensor_id_conf => $sensors_conf) {
            foreach ($sensors_conf as $cur_sensor_conf) {
                if (!isset($cur_sensor_conf['accuracy'])) {
                    $cur_sensor_conf['accuracy'] = null;
                }
                if (!isset($cur_sensor_conf['display'])) {
                    $cur_sensor_conf['display'] = null;
                }
                if (!isset($cur_sensor_conf['correction'])) {
                    $cur_sensor_conf['correction'] = null;
                }
                self::$ow_sensors[$cur_sensor_conf['alias']] = OwSensorFactory::getSensor(
                    $this->owConfig,
                    $this->input,
                    $cur_sensor_id_conf, 
                    $cur_sensor_conf['type'], 
                    $cur_sensor_conf['alias'],
                    $this->owConfig->separateur_decimal,
                    $cur_sensor_conf['accuracy'],
                    $cur_sensor_conf['display'],
                    $cur_sensor_conf['correction']
                );
                if ($this->input === 'csv') {
                    // on charge les données dans l'objet sans accéder à OwServer ni teleinfo
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_today_min = $minMax->get('today', 'min', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias());
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_today_min_date = $minMax->get('today', 'min', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias(), 'date');

                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_today_max = $minMax->get('today', 'max', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias());
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_today_max_date = $minMax->get('today', 'max', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias(), 'date');

                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_yesterday_min = $minMax->get('yesterday', 'min', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias());
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_yesterday_min_date = $minMax->get('yesterday', 'min', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias(), 'date');

                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_yesterday_max = $minMax->get('yesterday', 'max', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias());
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_yesterday_max_date = $minMax->get('yesterday', 'max', self::$ow_sensors[$cur_sensor_conf['alias']]->getAlias(), 'date');

                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure = $this->mesureCsv->get($cur_sensor_conf['alias'], MesureCsv::MESURE);
                    $mesures_trend = $this->mesureCsv->get($cur_sensor_conf['alias'], MesureCsv::MESURE_TREND);
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_trend = self::$ow_sensors[$cur_sensor_conf['alias']]->getTrend(
                        $mesures_trend['older_value'],
                        $mesures_trend['newer_value']
                    );
                    
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_yesterday = $this->mesureCsv->get($cur_sensor_conf['alias'], MesureCsv::MESURE_YESTERDAY);
                    self::$ow_sensors[$cur_sensor_conf['alias']]->mesure_lastweek = $this->mesureCsv->get($cur_sensor_conf['alias'], MesureCsv::MESURE_LASTWEEK);

                    $debug_message = $minMax->getDebugMessage();
                    if (count($debug_message) > 0) {
                        Util::log($this->log_file, implode("\n", $debug_message), __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
                    }
                }
            }
        }
        return $this->count();
    }

    /**
     * Vérifie si les sensors de la conf et du réseaux correspondent
     *
     * trigger_error si des incohérences sont remontées
     * 
     * @Exception OwSensorsException en cas d'erreur grave
     * @return boolean true si ok sinon Exception et error si simple alerte
     */
    protected function checkSensors(){
        if ($this->input === 'csv') {
            // @todo réfléchir à un test possible pour le mode CSV
            return true;
        }
        // récupération des ID des sensors sur le reseau ow
        try {
            $o_owNet = new owNet("tcp://".$this->owConfig->owserver_ip.":".$this->owConfig->owserver_port);
            $ret_get = $o_owNet->get("/", OWNET_MSG_DIR, true);
        } catch (Exception $e) {
            throw new OwSensorsConnexionException("Connexion au server OW impossible\n".$e->getMessage());
        }
        $a_sensors_ow = new OwSensorFilter(new OwSensorOwNetIterator($ret_get));

        $a_temp_sensors_id_ow = array();
        foreach ($a_sensors_ow as $cur_sensor_ow) {
            $a_temp_sensors_id_ow[$cur_sensor_ow['data_php']] = true;
            if(empty($this->owConfig->sensors[$cur_sensor_ow['data_php']])){
                trigger_error($cur_sensor_ow['data_php']." est sur reseau onewire mais pas dans owconfig.php", E_USER_NOTICE);
            }
        }

        foreach ($this->owConfig->sensors as $cur_sensor_id_conf => $sensors_conf) {
            if (empty($a_temp_sensors_id_ow[$cur_sensor_id_conf])){
                // on parcours les sondes de chaque sensor pour savoir si celui sur le réseaux à 
                // un équivalent dans le fichier de config
                foreach ($sensors_conf as $cur_sensor_conf) {
                    if ($cur_sensor_conf['type'] !== iOwSensor::TYPE_TELEINFO_PUISSANCE) {
                        // la sonde n'est pas une sonde teleinfo et elle n'est pas sur le rx onewire
                        trigger_error($cur_sensor_id_conf."' '".$cur_sensor_conf['alias']."' est dans owconfig.php mais pas sur le reseau onewire", E_USER_NOTICE);
                        unset(self::$ow_sensors[$cur_sensor_conf['alias']]);
                    }
                }
            }
        }
        if (count(self::$ow_sensors) == 0) {
            // Il n'y a plus aucun sensor sur le réseaux
            throw new OwSensorsSensorInitException("Les sensors n'ont pas été chargés correctement");
        }
        return true;
    }
    /**
     * Récupération d'une sensor spécifique
     *
     * @param string $sensor_alias  alias du sensor
     * @return OwSensor si la sensor correspondant à l'id spécifié existe sinon false lorsque le sensor n'est pas connu
     * @Exception Si les sensors n'ont pas été chargé alors une exception est levée
     */
    public function getSensor(string $sensor_alias){
        if(is_array($this::$ow_sensors) === false or count(self::$ow_sensors) == 0){
            throw new OwSensorsSensorInitException("Les sensors n'ont pas été chargés correctement");
        }
        if(isset(self::$ow_sensors[$sensor_alias]) and self::$ow_sensors[$sensor_alias] instanceof OwSensor){
            return self::$ow_sensors[$sensor_alias];
        }
        trigger_error ("Sensor '$sensor_alias' introuvable", E_USER_NOTICE);
        return false;
    }

    /**
     * Récupération des sensors
     *
     * @return array retourne un tableau de OwSensor ou un tableau vide si aucun n'est connu. 
     * Si les sensors n'ont pas été initialisés alors une exception est levée
     */
    public function getSensors(){
        if(is_array($this::$ow_sensors) === false or count(self::$ow_sensors) == 0){
            throw new OwSensorsSensorInitException("Les sensors n'ont pas été chargés correctement");
        }
        return $this::$ow_sensors;
    }

}
