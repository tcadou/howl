<?php
/**
 * Gestion de la récupération des données des sunset et sunrise
 * 
 * Pour rappel, l'api de yahoo donne pour la journée courante les valeurs de sunrise et sunset. Il faut donc appeler le service tous les jours pour récupérer les valeurs.
 *
 * La classe en PJ permet de sauvegarder ces données dans un fichier au format json.
 * <code>"2013/12/11":{"sr":"7:05", "ss":"17:13"},</code>
 *
 * Pour utiliser les infos dans ce fichier, il suffit donc de lire les lignes et de mettre des accolades autours pour avoir un json valide (et supprimer la dernière virgule).
 *
 * La classe fait bien-sûr l'ensemble du boulot :)
 *
 * Il faut pour finir créer un batch et le croner tous les jours pour sauvegarder les datas.
 * Il peut être lancé autant de fois que l'on veux le même jour, il se débrouille pour ne sauvegarder qu'une fois les données.
 */
class SunriseSunset{

	/** stock les coordonnées GPS du lieu */
	
	protected $location = null;
	/** diverses location prédefinies */
	const LOCATION_ST_SEBASTIEN_SUR_LOIRE_LATITUDE = 47.192844;
	const LOCATION_ST_SEBASTIEN_SUR_LOIRE_LONGITUDE = -1.510373;

	const ZENITH = 90.8333333333; //90+(50/60);

	private $date = null;
	private $sunrise = null;
	private $sunset = null;
	private $file_sunrisesunset = null;

	private $previous_date = null;
	private $previous_sunrise = null;
	private $previous_sunset = null;

	/** message de debug diponible après chaque appel de fonction */
	protected $debug_message = array();

	/**
	 * récupération de sunset sunrise
	 * 
	 * @param array $location latitude, longitude pour la location souhaitée
	 * @param string $file nom du fichier contenant les valeurs de sunset et sunrise.
	 * Le format du fichier est json mais Invalide car il manque les {} de début et fin.
	 */
	public function __construct($location, $output_file){
		$this->file_sunrisesunset = $output_file;
		if (!file_exists($this->file_sunrisesunset)) {
			touch($this->file_sunrisesunset);
		}
		if (!is_array($location)) {
			throw new Exception("Error Type ".print_r($location, true), 1);
		}
		$this->location = $location;
	}

	/** Récupère dans le fichier les dernières données sauvegardées */
	public function loadPrevious(){
		$temp_previous_sunrise_sunset = exec("tail -n 1 ".$this->file_sunrisesunset);
		if (empty($temp_previous_sunrise_sunset)) {
			return true;
		}
		$temp_previous_sunrise_sunset[strlen($temp_previous_sunrise_sunset) - 1] = '}';
		$previous_sunrise_sunset = json_decode('{'.$temp_previous_sunrise_sunset, true);
		$this->previous_date = key($previous_sunrise_sunset);
		$this->previous_sunrise = $previous_sunrise_sunset[$this->previous_date]['sr'];
		$this->previous_sunset = $previous_sunrise_sunset[$this->previous_date]['ss'];
		return true;
	}

	/** récupère les infos courante */
	public function loadCurrent(){
		$this->loadPrevious();
		$this->date = date('Y/m/d'); // données récupérées sont pour la date du jour
		if ($this->previous_date === $this->date) {
	        $this->debug_message[] = "sunrise sunset déjà sauvegardée pour cette date (".$this->date.") => on charge à partir du fichier.";
			$this->date = $this->previous_date;
			$this->sunrise = $this->previous_sunrise;
			$this->sunset = $this->previous_sunset;
			return true;
		}
        $this->debug_message[] = "Appel à date_sunrise() date_sunset() pour récupérer les sunrise sunset de cette date (".$this->date.")";
        // calcul du décalage avec GMT
		$dateTimeZoneCest = new DateTimeZone("CEST");
		//$dateTimeZoneUtc = new DateTimeZone("UTC");
		//$dateTimeCest = new DateTime("now", $dateTimeZoneCest);
		$dateTimeUtc = new DateTime("now", new DateTimeZone("UTC"));
		$timeOffset = $dateTimeZoneCest->getOffset($dateTimeUtc) / 3600;
		$this->sunrise = date_sunrise(
			time(),
			SUNFUNCS_RET_STRING, 
			$this->location[0], 
			$this->location[1], 
			self::ZENITH,
			$timeOffset
		);
		$this->sunset = date_sunset(
			time(),
			SUNFUNCS_RET_STRING, 
			$this->location[0], 
			$this->location[1], 
			self::ZENITH,
			$timeOffset
		);
		return true;
	}

	public function getCurrentDate(){
		return $this->date;
	}
	public function getCurrentSunrise(){
		return $this->sunrise;
	}
	public function getCurrentSunset(){
		return $this->sunset;
	}

	/**
	 * récupère les infos des n derniers jours en incluant le jour courant.
	 *
	 * Les données ne sont pas évaluées.
	 * Un json_decode est nécessaire pour les évaluer
	 *
	 * @param integer $nb_day les N derniers jours à charger en incluant le jour courant
	 * si nb_day=-1 alors on renvoi toutes les données.
	 * @return string les données au format json valide sous forme de chaine de caractère.
	 */
	public function load($nb_day=1){
		if ($nb_day == 0 or $nb_day < -1) {
	        $this->debug_message[] = "\$nb_day doit être > à 0 ou égale à -1";
	        return false;
		}
		if ($nb_day == -1) {
			$sunrise_sunset = file_get_contents($this->file_sunrisesunset);
		}else{
			$sunrise_sunset = shell_exec("tail -n ".$nb_day." ".$this->file_sunrisesunset);
		}
		if ($sunrise_sunset === null) {
			return true;
		}
		$sunrise_sunset[strlen($sunrise_sunset) - 1] = '}';
		return '{'.$sunrise_sunset;
	}


	/** Sauvegarde des données dans le fichier */
	public function save(){
		if ($this->previous_date === $this->date) {
	        $this->debug_message[] = "sunrise sunset déjà sauvegardée pour cette date (".$this->date.") => arrêt de la sauvegarde.";
			return true;
		}
        $this->debug_message[] = "Sauvegarde dans le fichier (".$this->file_sunrisesunset.")";
		file_put_contents(
			$this->file_sunrisesunset,
			"\n\"".$this->date.'":{"sr":"'.$this->sunrise.'", "ss":"'.$this->sunset.'"},',
			FILE_APPEND
		);
		$this->previous_date = $this->date;
		$this->previous_sunrise = $this->sunrise;
		$this->previous_sunset = $this->sunset;
		return true;
	}

	public function getDebugMessage(){
		$msg = $this->debug_message;
		$this->debug_message = null;
		return $msg;
	}

	/**
	 * fonction de test
	 */
	public static function test(){
		date_default_timezone_set('Europe/Paris');

		echo "### instanciation de la classe SunriseSunset\n";
		$srss = new self(
			array(
		    	self::LOCATION_ST_SEBASTIEN_SUR_LOIRE_LATITUDE,
		    	self::LOCATION_ST_SEBASTIEN_SUR_LOIRE_LONGITUDE
		    ),
			'/tmp/sunrise_sunset.json'
		);
		echo implode("\n", $srss->getDebugMessage())."\n";

		echo "### chargement des données du jour\n";
		if($srss->loadCurrent() == false){
			echo "Aucune données disponible actuellement pour aujourd'hui !";
		}
		echo implode("\n", $srss->getDebugMessage())."\n";

		echo "### Données récupérées :\n";
		echo "\t- ".$srss->getCurrentDate()."\n";
		echo "\t- ".$srss->getCurrentSunrise()."\n";
		echo "\t- ".$srss->getCurrentSunset()."\n";

		echo "### Sauvegarde\n";
		$srss->save();
		echo implode("\n", $srss->getDebugMessage())."\n";

		echo "### Chargement des 4 derniers jours :\n";
		echo $srss->load(4)."\n";
		echo @implode("\n", $srss->getDebugMessage())."\n";
	}
}

//SunriseSunset::test();