<?php
/**
 * Gestion des mesures minMax dans le daemon seconde
 */
class MinMax{

	/** fichier contenant les min max pour l'heure courante */
	protected $f_minmax_heure = null;
	/** fichier contenant les min max pour la journée courante */
	protected $f_minmax_today = null;
	/** fichier contenant les min max pour la journée d'hier */
	protected $f_minmax_yesterday = null;

	/** min max courants */
	protected $minmax_heure = null;
	/** min max courants */
	protected $minmax_today = null;
	/** min max courants */
	protected $minmax_yesterday = null;

	/** message de debug diponible après chaque appel de fonction */
	protected $debug_message = array();

	/**
	 * définition des paramètres initiaux
	 *
	 * @param string $f_minmax_heure nom du fichier contenant les minmax de l'heure courante
	 * @param string $f_minmax_today nom du fichier contenant les minmax de la journée courante
	 * @param string $f_minmax_yesterday nom du fichier contenant les minmax de la journée d'hier
	 * 
	 */
	public function __construct($f_minmax_heure, $f_minmax_today, $f_minmax_yesterday){
		$this->f_minmax_heure = $f_minmax_heure;
		$this->f_minmax_today = $f_minmax_today;
		$this->f_minmax_yesterday = $f_minmax_yesterday;
		$this->init('all', false);
	}

	/**
	 * initialiation des minmax
	 *
	 * @param string $periode periode concernée par l'initiliation des minmax : 'all' (default), 'heure', 'jour'.
	 * @param boolean $force force ou non l'initialisation. par defaut, l'initialisation ne peut se faire qu'une seule et unique fois.
	 */
	public function init($periode='all', $force = false){
		static $deja_passe = false;

		if ($deja_passe === true and $force === false) {
            $this->debug_message[] = "Initialisation des minmax déjà réalisée";
			return false;
		}
		$deja_passe = true;

        clearstatcache();

	    $flag_minmax_heure_loaded = true;
	    $flag_minmax_jour_loaded = true;
        // initialisation des min max HEURE
		if($periode == 'all' or $periode == 'heure'){
	        if (file_exists($this->f_minmax_heure) === true 
	            and filemtime($this->f_minmax_heure) > mktime(date("H"), 0, 0, date("n"), date("j"), date("Y"))
	            ) {
	            // on essaye de récupérer les données min max de l'heure à partir du fichier si elles sont suffisamment récentes
	            // sinon, on utilisera les mesures courantes un peu plus tard
	            // si le fichier a été modifié après la dernière heure pile, alors on récupère la donnée sinon on prend la valeur courante
	            $this->debug_message[] = "Initialisation des minmax HEURE à partir du fichier ".$this->f_minmax_heure;
	            $this->minmax_heure = unserialize(file_get_contents($this->f_minmax_heure));
	            if ($this->minmax_heure === false) {
	            	$this->minmax_heure = null;
		            $this->debug_message[] = "Échec de l'initialisation des minmax HEURE";
				    $flag_minmax_heure_loaded = false;
	            }
	        }
	    }

        // initialisation des min max JOUR
		if($periode == 'all' or $periode == 'jour'){
	        if (file_exists($this->f_minmax_today) === true
	            and filemtime($this->f_minmax_today) > mktime(0, 0, 0, date("n"), date("j"), date("Y"))
	            ) {
	            // si le fichier a été modifié après minuit de la journée courante, alors on récupère la donnée sinon on prend la valeur courante
	            $this->debug_message[] = "Initialisation des minmax JOUR à partir du fichier ".$this->f_minmax_today;
	            $this->minmax_today = unserialize(file_get_contents($this->f_minmax_today));
	            if ($this->minmax_today === false) {
	            	$this->minmax_today = null;
		            $this->debug_message[] = "Échec de l'initialisation des minmax JOUR pour le fichier ".$this->f_minmax_today;
				    $flag_minmax_jour_loaded = false;
	            }
		    }
	        if (file_exists($this->f_minmax_yesterday) === true
	            and filemtime($this->f_minmax_yesterday) > (mktime(0, 0, 0, date("n"), date("j"), date("Y")) - 3600)
	            ) {
	            // si le fichier a été modifié aux alentours de minuit de la journée courante, alors on récupère la donnée
	            // sinon échec
	            $this->debug_message[] = "Initialisation des minmax JOUR à partir du fichier ".$this->f_minmax_yesterday;
	            $this->minmax_yesterday = unserialize(file_get_contents($this->f_minmax_yesterday));
	            if ($this->minmax_yesterday === false) {
	            	$this->minmax_yesterday = null;
		            $this->debug_message[] = "Échec de l'initialisation des minmax JOUR pour le fichier ".$this->f_minmax_yesterday;
				    $flag_minmax_jour_loaded = false;
	            }
		    }
		}
	    return ($flag_minmax_jour_loaded and $flag_minmax_heure_loaded);
	}

	/**
	 * restitution des minmax
	 *
	 * @param string $periode periode souhaitée 'heure', 'today' ou 'yesterday'.
	 * @param string $level niveau souhaité 'min' ou 'max'
	 * @param string $mesure_name nom de la sonde
	 * @param string $type permet de récupérer la 'mesure' ou la 'date' demandé. Par défaut on retourne la mesure
	 * @return integer|null la valeur si elle existe sinon null
	 */
	public function get($periode, $level, $mesure_name, $type='mesure'){
		// $this->${"minmax_$periode"}[$level][$mesure_name]
		if ($periode == 'today' and isset($this->minmax_today[$level][$mesure_name][$type])) {
			$this->debug_message[] = "$periode, $level, $mesure_name => ".$this->minmax_today[$level][$mesure_name][$type];
			return $this->minmax_today[$level][$mesure_name][$type];
		}elseif ($periode == 'yesterday' and isset($this->minmax_yesterday[$level][$mesure_name][$type])) {
			$this->debug_message[] = "$periode, $level, $mesure_name => ".$this->minmax_yesterday[$level][$mesure_name][$type];
			return $this->minmax_yesterday[$level][$mesure_name][$type];
		}elseif ($periode == 'heure' and isset($this->minmax_heure[$level][$mesure_name][$type])) {
			$this->debug_message[] = "$periode, $level, $mesure_name => ".$this->minmax_heure[$level][$mesure_name][$type];
			return $this->minmax_heure[$level][$mesure_name][$type];
		}
		$this->debug_message[] = "$periode, $level, $mesure_name => NULL";
		return null;
	}

	/**
	 * Unchangement d'heure ou de jour réinitialise les minmax + rotation des fichiers min max jour
	 *
	 * @param string $periode periode concernée par la réinitialisation des minmax : 'all' (default), 'heure', 'jour'.
	 */
	public function changementDe($periode='all'){
		if($periode == 'all' or $periode == 'heure'){
            $this->debug_message[] = "Reset des minmax HEURE";
			$this->minmax_heure = null;
		}
		if($periode == 'all' or $periode == 'jour'){
            $this->debug_message[] = "Reset des minmax JOUR et rotation today/yesterday";
			$this->minmax_today = null;
	        @copy($this->f_minmax_today, $this->f_minmax_yesterday);
        }
	}

	/**
	 * maj des minmax
	 *
	 * @param array $mesure nouvelle mesure visant à mettre à jour au besoin les min max courants.
	 * Format :<code>
	 * $mesure = [$mesure_name] => $valeur
	 * </code
	 * @param string $date_time date correspondant aux mesures
	 * @return boolean true si min max mis à jour sinon false
	 */
	public function update($mesure, $date_time){
        $flag_minmax_heure_changed = false;
        $flag_minmax_jour_changed = false;
	    foreach ($mesure as $cur_mesure_name => $cur_mesure) {
	    	if (is_array($cur_mesure) === true) {
				// les minmax des mesures multivaleurs sont basées sur la mesure seconde
				$cur_mesure = $cur_mesure[BatchMesure::CHANGEMENT_SECONDE];
	    	}
	    	// MIN HEURE
			if (!is_null($cur_mesure) 
				and (	(	isset($this->minmax_heure['min'][$cur_mesure_name]['mesure']) // on a un min
	        				and $this->minmax_heure['min'][$cur_mesure_name]['mesure'] > $cur_mesure // > à la nouvelle mesure
	        			)
	        			or
	        			(	!isset($this->minmax_heure['min'][$cur_mesure_name]['mesure']) // pas encore de min
	        			)
	        		)
        		){ // update
	        	if (isset($this->minmax_heure['min'][$cur_mesure_name]['mesure'])) {
	        		$this->debug_message[] = "HEURE-MIN ".$this->minmax_heure['min'][$cur_mesure_name]['mesure']." > ".$cur_mesure." ($cur_mesure_name)";
	        	}else{
	        		$this->debug_message[] = "HEURE-MIN initialisation ".$cur_mesure." ($cur_mesure_name)";
	        	}
	            $this->minmax_heure['min'][$cur_mesure_name] = array('date' => $date_time, 'mesure' => $cur_mesure);
	            $flag_minmax_heure_changed = true;
			}

	        // MAX HEURE
			if (!is_null($cur_mesure) 
				and (	(	isset($this->minmax_heure['max'][$cur_mesure_name]['mesure']) // on a un max
	        				and $this->minmax_heure['max'][$cur_mesure_name]['mesure'] < $cur_mesure // < à la nouvelle mesure
	        			)
	        			or
	        			(	!isset($this->minmax_heure['max'][$cur_mesure_name]['mesure']) // pas encore de max
	        			)
	        		)
        		){ // update
	        	if (isset($this->minmax_heure['max'][$cur_mesure_name]['mesure'])) {
	        		$this->debug_message[] = "HEURE-MAX ".$this->minmax_heure['max'][$cur_mesure_name]['mesure']." < ".$cur_mesure." ($cur_mesure_name)";
	        	}else{
	        		$this->debug_message[] = "HEURE-MAX initialisation ".$cur_mesure." ($cur_mesure_name)";
	        	}
	            $this->minmax_heure['max'][$cur_mesure_name] = array('date' => $date_time, 'mesure' => $cur_mesure);
	            $flag_minmax_heure_changed = true;
			}

		    if ($flag_minmax_heure_changed === true) {
		        file_put_contents($this->f_minmax_heure, serialize($this->minmax_heure));
		    }

	    	// MIN JOUR
			if (!is_null($cur_mesure) 
				and (	(	isset($this->minmax_today['min'][$cur_mesure_name]['mesure']) // on a un min
	        				and $this->minmax_today['min'][$cur_mesure_name]['mesure'] > $cur_mesure // > à la nouvelle mesure
	        			)
	        			or
	        			(	!isset($this->minmax_today['min'][$cur_mesure_name]['mesure']) // pas encore de min
	        			)
	        		)
        		){ // update
	        	if (isset($this->minmax_today['min'][$cur_mesure_name]['mesure'])) {
		            $this->debug_message[] = "JOUR-MIN ".@$this->minmax_today['min'][$cur_mesure_name]['mesure']." > ".$cur_mesure." ($cur_mesure_name)";
		        }else{
		        	$this->debug_message[] = "JOUR-MIN initialisation ".$cur_mesure." ($cur_mesure_name)";
		        }
	            $this->minmax_today['min'][$cur_mesure_name] = array('date' => $date_time, 'mesure' => $cur_mesure);
	            $flag_minmax_jour_changed = true;
	        }
	        
	    	// MAX JOUR
			if (!is_null($cur_mesure) 
				and (	(	isset($this->minmax_today['max'][$cur_mesure_name]['mesure']) // on a un max
	        				and $this->minmax_today['max'][$cur_mesure_name]['mesure'] < $cur_mesure // < à la nouvelle mesure
	        			)
	        			or
	        			(	!isset($this->minmax_today['max'][$cur_mesure_name]['mesure']) // pas encore de max
	        			)
	        		)
        		){ // update
	        	if (isset($this->minmax_today['max'][$cur_mesure_name]['mesure'])) {
		            $this->debug_message[] = "JOUR-MAX ".@$this->minmax_today['max'][$cur_mesure_name]['mesure']." < ".$cur_mesure." ($cur_mesure_name)";
		        }else{
		        	$this->debug_message[] = "JOUR-MAX initialisation ".$cur_mesure." ($cur_mesure_name)";
		        }
	            $this->minmax_today['max'][$cur_mesure_name] = array('date' => $date_time, 'mesure' => $cur_mesure);
	            $flag_minmax_jour_changed = true;
	        }
		    // sauvegarde des nouveaux minmax
		    if ($flag_minmax_jour_changed === true) {
		        file_put_contents($this->f_minmax_today, serialize($this->minmax_today));
		    }    
	    }
	    return ($flag_minmax_jour_changed or $flag_minmax_heure_changed);
	}

	public function getDebugMessage(){
		$msg = $this->debug_message;
		$this->debug_message = array();
		return $msg;
	}
}