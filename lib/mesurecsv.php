<?php

/**
 * Gestion des mesures dans le format CSV
 */

 class MesureCsv {

 	const TREND_UP = 'increase';
 	const TREND_DOWN = 'decrease';
 	const TREND_STABLE = 'stable';

 	const MESURE = 'mesure';
 	const MESURE_TREND = 'mesure_trend';
 	const MESURE_YESTERDAY = 'mesure_yesterday';
 	const MESURE_LASTWEEK = 'mesure_lastweek';

	protected $mesure_header = array();

	protected $data = array();

	public function __construct($owConfig){
		$this->owConfig = $owConfig;
		// récupération de l'entête
		$this->loadHeader();
	}

    public function set($sensor_alias, $name, $value){
        $this->data[$name][$sensor_alias] = $value;
    }

    public function get($sensor_alias, $name){
        if (isset($this->data[$name][$sensor_alias])) {
            return $this->data[$name][$sensor_alias];
        }
        return null;
    }

	/**
	 * charge les données à partir des fichiers CSV
	 */
 	public function loadMesures(){
 		// mesure courantes from mesure_seconde dernière ligne
 		$this->data[self::MESURE] = self::getLastMesures($this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_seconde);
 		// tendances
 		$this->data[self::MESURE_TREND] = $this->getMesuresForTrend();
 		// yesterday
 		$this->data[self::MESURE_YESTERDAY] = $this->getMesureYesterday();
 		// lastweek
 		$this->data[self::MESURE_LASTWEEK] = $this->getMesureLastWeek();
        return true;
 	}

 	/**  charge les entêtes d'un fichiers CSV contenant le nom des sondes */
	public function loadHeader(){
		if (!empty($this->mesure_header)) {
	        return $this->mesure_header;
		}
		$this->mesure_header = array('Date');
        foreach ($this->owConfig->sensors as $cur_sensor_id_conf => $sensors_conf) {
            foreach ($sensors_conf as $cur_sensor_conf) {
                $this->mesure_header[] = $cur_sensor_conf['alias'];
            }
        }
        return $this->mesure_header;
	}

	/**
	 * Charges les mesures old et last pour calculer les tendances à partir du fichier minute
	 * 
	 * Cette méthode doit être appelée après self::getLastMesures()
	 *
	 */
	protected function getMesuresForTrend(){
 		$mesure_trend = array();
		// chargement des mesures à partir du fichier minute
        $m2 = $this->stringToArray(self::getLastLine($this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_minute, 1));

 		$trente_minute_avant = strtotime($m2['Date']) - (30*60); // 30 minute avant la date courante
 		unset($m2['Date']);

 		$m1 = $this->stringToArray(self::getLineMatchDate(
 			$this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_minute, 
 			$trente_minute_avant,
 			60,
 			$this->owConfig->fichier_mesure_minute_taille_ligne
 		));
 		unset($m1['Date']);

 		foreach ($m1 as $key => $mesure) {
			$mesure_trend[$key] = array(
				'older_value' => $m1[$key],
				'newer_value' => $m2[$key]
			);
 		}
        return $mesure_trend;
	}

	/**
	 * Retourne les dernières mesure (la dernière ligne du fichier CSV passé en paramètre)
	 * Les clés sont construites à partir de l'entête du fichier
	 * @return un tableau associatifs avec les données de la dernière ligne.
	 * /!\ Un contrôle est réalisé sur les données. Si elles sont périmées (> à 66 minutes) on retourne des valeurs NULL.
	 */
	protected function getLastMesures($file){
		// récupération des dernières mesures
        $ret_lastline = $this->stringToArray(self::getLastLine($file));
        // données périmées ?
        if (time() - strtotime($ret_lastline['Date']) > 4000) {
        	foreach ($ret_lastline as $key => $value) {
        		$ret_lastline[$key] = null;
        	}
			$ret_lastline['Date'] = date('Y/m/d H:i:s', time()); // on réinjecte la date courante dans le tableau null
        }
        return $ret_lastline;
	}

	/**
	 * Retourne les mesures d'hier à la même heure
	 * Les clés sont construites à partir de l'entête du fichier
	 */
	protected function getMesureYesterday(){
		// récupération des dernières mesures
		$yesterday = strtotime($this->data[self::MESURE]['Date']) - (24*60*60); // hier
 		$ret_getLineMatchDate_yesterday = self::getLineMatchDate(
 			$this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_minute, 
 			$yesterday,
 			60,
 			$this->owConfig->fichier_mesure_minute_taille_ligne
 		);
 		$ligne_yesterday_array = $this->stringToArray($ret_getLineMatchDate_yesterday);
 		if($ret_getLineMatchDate_yesterday === false){
			$ligne_yesterday_array['Date'] = date('Y/m/d H:i:s', $yesterday); // on réinjecte la date dans le tableau null
 		}
 		return $ligne_yesterday_array;
	}

	/**
	 * Retourne les mesures de la semaine dernière à la même heure
	 * Les clés sont construites à partir de l'entête du fichier
	 */
	protected function getMesureLastWeek(){
		$lastweek = strtotime($this->data[self::MESURE]['Date']) - (7*24*60*60); // il y a 1 semaine
 		$ret_getLineMatchDate_lastweek = self::getLineMatchDate(
 			$this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_heure, 
 			$lastweek,
 			3600,
 			$this->owConfig->fichier_mesure_heure_taille_ligne
 		);
 		$ligne_lastweek_array = $this->stringToArray($ret_getLineMatchDate_lastweek);
 		if($ret_getLineMatchDate_lastweek === false){
			$ligne_lastweek_array['Date'] = date('Y/m/d H:i:s', $lastweek); // on réinjecte la date dans le tableau null
 		}else{
			foreach ($ligne_lastweek_array as $key => $value) {
		 		if (strpos($value, ';') !== false) {
					list( , $ligne_lastweek_array[$key], ) = explode(';', $value);
				}
			}
 		}
        return $ligne_lastweek_array;
	}


	/**
	 * Transforme une ligne CSV en tableau en interprétant chacune des valeurs.
	 *
	 * Le cas échéant, cette fonction remplace les "Nan" par des null voir un tableau complet de null au besoin
	 */
	public function stringToArray($line_string){
		// cas ou la ligne n'a pas été récupérée correctement auparavant
        if ($line_string === false) {
			$line_string = array_fill(0, count($this->mesure_header), null);
	        return array_combine(
	        	$this->mesure_header,
	        	$line_string
	        );
        }
        $line_array = explode(',', $line_string);
        // test les incohérence entre le titre et le nombre de valeur
        if (count($this->mesure_header) != count($line_array)) {
			Util::log(HOWL_LOGFILE, "Le nombre d'élément du titre et de la ligne de mesure ne correspondent pas !\nIl faut corriger les fonctions d'écriture CSV !\n".implode(',', $line_array), __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
			$line_array = array_fill(0, count($this->mesure_header), null);
	        return array_combine(
	        	$this->mesure_header,
	        	$line_array
	        );
        }
        // on remplace les Nan par des NULL
        foreach ($line_array as $key => $value) {
        	if ($value == 'Nan') {
        		$line_array[$key] = null;
        	}
        }
        return array_combine(
        	$this->mesure_header,
        	$line_array
        );
	}

	/** Retourne la date sous forme de timestamp de la dernière ligne du fichier CSV passé en paramètre */
	public static function getTimestampFromLastLine($file){
        $ret_lastline = explode(',', self::getLastLine($file));
        return strtotime($ret_lastline[0]);
	}

	/** Retourne la date sous forme de timestamp de la ligne passée en paramètre sous forme de string */
	public static function getTimestampFromStringLine($line){
        $ret_line = explode(',', $line);
        return strtotime($ret_line[0]);
	}

	/** Retourne les dernières lignes du fichier passé en paramètre */
	public static function getLastLine($file, $number_of_line=1){
		$ret_exec = exec('tail -n '.$number_of_line.' '.$file, $output);
		if ($number_of_line == 1) {
			return $ret_exec;
		}
        return $output;
	}

	/**
	 * Supprime les lignes les plus anciennes pour garder le nombre de ligne cible.
	 * 
	 * @uses les appels systèmes suivants seront utilisés wc/tail/head
	 *
	 * @param string $file fichier à traiter
	 * @param integer $nb_max_line nombre de ligne qu'il doit rester dans le fichier après réduction
	 * @param boolean|string|array $entete entete à injecter dans le nouveau fichier
	 * - false : ne pas gérer l'entête
	 * - true : reprendre l'entete du premier fichier
	 * - string : la chaîne fournie est a considérer comme la nouvelle entête
	 * - array : implode avec une virgule commme glue
	 * @param boolean $backup réalise un backup ou non du fichier avant réduction
	 * @return integer|boolean Le nombre de ligne avant atteinte de la taille cible (> à $nb_max_line) ou true si le resize a été réalisée
	 * 
	 */
	public static function resizeFile($file, $nb_max_ligne, $entete=false, $backup=false){
	    $nb_ligne = exec('wc -l '.$file);
	    if ($entete === false) {
	    	$nb_ligne++;
	    }
	    if($nb_ligne > $nb_max_ligne){
	        if($entete === true){
				exec('head -n 1 '.$file.' > '.$file.'.new');
	            file_put_contents($file.'.new', "\n", FILE_APPEND);
	        }elseif(is_array($entete)){
	            file_put_contents($file.'.new', implode(',', $entete)."\n");
	        }elseif(is_string($entete) === true){
	            file_put_contents($file.'.new', $entete."\n");
	        }else{
	            touch($file.'.new');
	        }
	        exec('tail -n '.$nb_max_ligne.' '.$file.' >> '.$file.'.new');
	        if ($backup === true) {
		        @unlink($file.'.bkp');
	        	rename($file, $file.'.bkp');
	        }else{
		        unlink($file);
	        }
	        rename($file.'.new', $file);
	        return true;
	    }else{
	    	// il faut ajouter au moins le nombre de ligne ci-dessous pour qu'un resize soit réalisé
	        return $nb_max_ligne - $nb_ligne + 1;
	    }
	}


	/**
	 * Écrit au format CSV
	 * Les valeurs NULL sont remplacées par des "Nan" afin de créer des trous dans les graph
	 * @param string $csv_file nom du fichier cible
	 * @param string données à écrire dans le fichier
	 * @param integer nombre de données contenu dans $data. Si présent, un contrôle sera réalisé et les données seront complétées.
	 * @param char $delimiter séparateur
	 * @param char $enclosure encadre chaque données
	 */
	public static function fwrite($csv_file, $data, $nb_element_to_write=null, $delimiter=',', $enclosure=''){
    	$prefixe = "\n";
        if (file_exists($csv_file) === false or filesize($csv_file) === 0){
        	$prefixe = "";
        }
        if (!is_array($data)) {
        	// données d'entrée invalide
        	if ($nb_element_to_write !== null) {
        		// on insère QUE des données "Nan"
		        $data = array_fill(0, $nb_element_to_write, "Nan");
				Util::log(HOWL_LOGFILE, "Les données à écrire dans le fichier $csv_file sont invalides. \nDes valeurs 'Nan' ont été écrites à la place :\n".print_r($data, true), __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
        	}else{
        		// on écrit rien dans le fichier
				Util::log(HOWL_LOGFILE, "Les données à écrire dans le fichier $csv_file sont invalides. \nAucune écriture n'a été réalisée :\n".print_r($data, true), __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
        	}
        }elseif ($nb_element_to_write !== null) {
        	// on complète le tableau
        	if (count($data) < $nb_element_to_write) {
        		// il manque des données, on complète
		        $data = array_pad($data, $nb_element_to_write, "Nan");
				Util::log(HOWL_LOGFILE, "Les données à écrire dans le fichier $csv_file sont invalides. \nIl manque des données, on complète avec des 'Nan' :\n".print_r($data, true), __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
        	}elseif (count($data) > $nb_element_to_write) {
        		// on insère QUE des données "Nan"
		        $data = array_fill(0, $nb_element_to_write, "Nan");
				Util::log(HOWL_LOGFILE, "Les données à écrire dans le fichier $csv_file sont invalides. \nDes valeurs 'Nan' ont été écrites à la place :\n".print_r($data, true), __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
        	}
        }
        foreach ($data as $key => $value) {
        	if ($value === null) {
        		$data[$key] = 'Nan';
        	}
        }
		return file_put_contents($csv_file, $prefixe.$enclosure.implode($enclosure.$delimiter.$enclosure, $data).$enclosure, FILE_APPEND);
	}


	/**
	 * retourne la ligne commençant par la date la plus proche
	 *
	 * on n'estime pas a taille des lignes, on la donne car c'est fixe. Elle est dans le fichier de config.
	 *
	 * @param string|SplFileObject $file nom du fichier dans lequel on effectue la recherche ou handler sur ce fichier
	 * @param integer $target_date timestamp de la date recherchée
	 * @param integer $periode duree theorique (en seconde) entre 2 lignes (10, 60, 3600)
	 * @param integer taille de la ligne en octet (correpsond dans notre cas au nombre de caractère sur chaque ligne +1)
	 * @return string|false Ligne recherchée ou false si elle n'est pas trouvée
	 */
	public static function getLineMatchDate($fh, $target_date, $periode, $line_size, $target_offset_current=null, $target_offset_max=null, $target_offset_min=null){
		static $message = '';
		static $best_line_so_far = null;

		//initialisation de la fonction
		if ($target_offset_current === null and $target_offset_max === null and $target_offset_min === null) {
			$file = $fh;
			unset($fh);
			$fh = new SplFileObject($file);
		    $date_last_line = self::getTimestampFromLastLine($file);
			$message .= "# INIT getLineMatchDate() : Recherche de \"".date('Y/m/d H:i:s', $target_date)."\" dans le fichier $fh (periode=$periode, line_size=$line_size, date_last_line=".date('Y/m/d H:i:s', $date_last_line).")\n";

			ob_start(); 
			debug_print_backtrace(); 
			$trace = ob_get_contents(); 
			ob_end_clean(); 

			$message .= "\t".str_replace("\n", "\n\t", $trace)."\n";
			$target_offset_current = ( ( $date_last_line - $target_date ) / $periode ) * $line_size;
			return self::getLineMatchDate(
				$fh,
				$target_date,
				$periode,
				$line_size,
				$target_offset_current,
				null, // taille du fichier entier mais null est gérée de manière équivalente
				0
			);
		}
		$message .= "\t# SEARCH : target_date=".date('Y/m/d H:i:s', $target_date).", target_offset_current=$target_offset_current, target_offset_max=$target_offset_max, target_offset_min=$target_offset_min\n";
		// on lance la recherche
		$fh->fseek(- $target_offset_current, SEEK_END); 
		$fh->current();
		$fh->next();
		$date_seeker = self::getTimestampFromStringLine($fh->current());
		$message .= "\t# CURRENT LINE : ".trim($fh->current())."\n";
		$target_date_delta = $target_date - $date_seeker;
		// on sauvegarde la ligne courante si le delta est meilleure que la dernière ligne analysée
		if ($best_line_so_far === null or $best_line_so_far[0] > abs($target_date_delta)) {
			$best_line_so_far = array(abs($target_date_delta), trim($fh->current()));
		}
		$message .= "\t# DELTA_DATE : $target_date_delta\n";
		if( abs( $target_date_delta ) <= ( ($periode * 2) / 3) ) {
			// la date de la ligne courante est égale à $target_date +- $periode
			// donc on est sur la ligne recherchée car la plus proche possible de la date cible
			$message = '';
			$best_line_so_far = null;
			return trim($fh->current());
		}
		// le delta_offset entre l'ancien et le nouveau doit obligatoirment être au minimum de $line_size
		// ce qui revient à un target_date_delta au moins égale à $periode
		if(abs($target_date_delta) < $periode) {
			if ($target_date_delta > 0) {
				$target_date_delta = $periode;
			}else{
				$target_date_delta = -$periode;
			}
		}
		// calcul du nouvel offset car la ligne courante est trop éloignée de la date target
		$target_offset_new = round($target_offset_current - ( ( $target_date_delta / $periode ) * $line_size ));

		if (($target_offset_new >= $target_offset_max and $target_offset_max != null) or $target_offset_new <= $target_offset_min) {
			if ($target_date_delta < 0) {
				$test_target_offset_min = $target_offset_current;
				$test_target_offset_max = $target_offset_max;
			}else{
				$test_target_offset_max = $target_offset_current;
				$test_target_offset_min = $target_offset_min;
			}
			if ($test_target_offset_max - $test_target_offset_min > $line_size) {
				$message .= "\t# OFFSET DEJA ANALYSEE, Il reste des lignes à analyser (on utilise la dicotomie)\n";
				$target_offset_new = ($test_target_offset_max + $test_target_offset_min) / 2;
			}else{
				// le nouvel offset a déjà été scanné.
				// c'est possible si la valeur recherchée n'existe pas (il y a des trous).
				// si le target_date_delta est "raisonnable" alors on retourne la ligne courante, sinon on retourne false
				/**///if ($target_date - $date_seeker > $periode * 2) {
				if($best_line_so_far[0] > $periode * 2) {
					//$message .= "\t# OFFSET DEJA ANALYSEE, La ligne courante est trop éloignée de la cible, échec de la recherche.\n";
					$message .= "\t# OFFSET DEJA ANALYSEE.\n";
					$message .= "\t# BEST LINE, ".$best_line_so_far[1].".\n";
					// on désactive le log complet dans le fichier
					$message = "\t# ECHEC de la recherche : La ligne la plus proche est trop éloignée de la cible.\n";
					Util::log(HOWL_LOGFILE, $message, __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
					$message = '';
					$best_line_so_far = null;
					return false;
				}
				// on test si la ligne retournée ne contient que des "Nan" (équivalent à pas de chiffres à virgule)
				//if (strpos($fh->current(), '.') === false and strpos($fh->current(), 'Nan') !== false) {
				if (strpos($best_line_so_far[1], '.') === false and strpos($best_line_so_far[1], 'Nan') !== false) {
					$message .= "\t# OFFSET DEJA ANALYSEE.\n";
					$message .= "\t# BEST LINE, ".$best_line_so_far[1].".\n";
					// on désactive le log complet dans le fichier
					$message = "\t# ECHEC de la recherche : La ligne la plus proche est vide (Nan).\n";
					Util::log(HOWL_LOGFILE, $message, __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
					$message = '';
					$best_line_so_far = null;
					return false;
				}
				// on retourne la "best line" car elle n'est pas trop éloignée de la cible
				$message = '';
				$line_to_return = $best_line_so_far[1];
				$best_line_so_far = null;
				return $line_to_return;
			}
		}

		// on ressert la fenêtre de recherche
		if ($target_date_delta < 0) {
			$target_offset_min = $target_offset_current;
		}else{
			$target_offset_max = $target_offset_current;
		}

		// on recherche sur le nouvel offset
		return self::getLineMatchDate(
			$fh,
			$target_date,
			$periode,
			$line_size,
			$target_offset_new,
			$target_offset_max,
			$target_offset_min
		);
	}

}

