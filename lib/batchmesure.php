<?php
/**
 * gère le batch mesure.php
 */
class BatchMesure{

    const CHANGEMENT_JOURNEE = 'changement_journee';
    const CHANGEMENT_HEURE = 'changement_heure';
    const CHANGEMENT_MINUTE = 'changement_minute';
    const CHANGEMENT_SECONDE = 'changement_seconde';

	/** détermine si la boucle courante se produit durant un changement de journée */
	protected $flag_nouvelle_journee = false;

	/** détermine si la boucle courante se produit durant un changement d'heure */
	protected $flag_nouvelle_heure = false;
	/** gestion des changement d'heure */
	protected $heure_precedente = null;
	/** gestion des changement d'heure */
	protected $heure_courante = null;

	/** détermine si la boucle courante se produit durant un changement de minute */
	protected $flag_nouvelle_minute = false;
	/** gestion des changement de minute */
	protected $minute_precedente = null;
	/** gestion des changement de minute */
	protected $minute_courante = null;

	/** gestion des problèmes de connexion */
	protected $pb_connexion = 0;
	/** gestion des problèmes de connexion */
	protected $date_pb_connexion = null;

	protected $owServer = null;

	/** gestion de la rotation des mesures dans le fichier csv seconde */
	protected $nb_mesure_avant_resize_fichier_seconde = null;
	/** gestion de la rotation des mesures dans le fichier csv minute */
	protected $nb_mesure_avant_resize_fichier_minute = null;

	/** fichier de log */
	protected $log_file = null;

	/** configuration du batch */
	protected $owConfig = null;

	/** Objet gérant les valeurs min et max */
	protected $minMax = null;

	/** entete des fichiers CSV (date + alias des sensors) */
	protected $entete_csv = array();

	/** template de mesure vide dans le CSV seconde et minute */
	protected $mesure_null = null;
	/** template de mesure vide dans le CSV heure */
	protected $mesure_null_heure = null;

	/** mesures de la boucle courante */
	protected $mesures_courantes = array();

	/** mode verbeux */
	protected $verbose=false;

	/**
	 * mode d'exécution du batch (display|csv)
	 * le mode csv se comporte comme un daemon
	 */
	protected $output_mode = 'display';

	/** fichier de sortie seconde */
	protected $fichier_mesure_seconde = null;
	/** fichier de sortie minute */
	protected $fichier_mesure_minute = null;
	/** fichier de sortie heure */
	protected $fichier_mesure_heure = null;

	/**
	 * 
	 * @param OwConfig $owConfig configuration du batch
	 * @param string $log_file fichier de log
	 *
	 */
	public function __construct($owConfig, $log_file, $verbose=false){
		$this->log_file = $log_file;
		Util::log($this->log_file, "Lancement du daemon", __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);

		$this->owConfig = $owConfig;
		$this->verbose = $verbose;

		$this->fichier_mesure_seconde = $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_seconde;
		$this->fichier_mesure_minute = $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_minute;
		$this->fichier_mesure_heure = $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_mesure_heure;

		// initialisation des resize des fichiers mesure
		$this->nb_mesure_avant_resize_fichier_minute = $this->nb_mesure_avant_resize_fichier_seconde = $this->owConfig->freq_analyse_file_size;

		$this->checkArguments();
		$this->initOwServerConnexion();
		if ($this->output_mode == 'csv') {
			$this->initMinMax();
			$this->initCsvFIle();
		}
	}

	public function run(){
		Util::log($this->log_file, "Début boucle de mesure. Fréquence ".$this->owConfig->daemon_mesure_frequence." secondes", __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
		while (true) {
			if ($this->output_mode == 'csv') {
				$this->resizeFileSeconde();
				$this->resizeFileMinute();
			}
		    // récupération de l'heure courante pour avoir un horodatage commun aux différents fichiers
		    $date_time = $this->owConfig->getTime();
		    // maj des changements horaires
			$this->checkTimeChanges($date_time);

			// récupération des mesures
			if ($this->flag_nouvelle_journee === true) {
				$flag_changement_periode = BatchMesure::CHANGEMENT_JOURNEE;
			}elseif ($this->flag_nouvelle_heure === true) {
				$flag_changement_periode = BatchMesure::CHANGEMENT_HEURE;
			}elseif ($this->flag_nouvelle_minute === true) {
				$flag_changement_periode = BatchMesure::CHANGEMENT_MINUTE;
			}else{
				$flag_changement_periode = BatchMesure::CHANGEMENT_SECONDE;
			}
			$different_than_last_time = $this->getMesure($flag_changement_periode);

			// maj des min et max
		    $this->updateMinMax($date_time);

		    // écriture des csv
		    if ($this->flag_nouvelle_heure === true) {
				$this->writeCsvFileHeure($date_time);
		    }
		    // Nouvelle journée donc on réinitialise les minmax JOUR
		    if($this->flag_nouvelle_journee === true){
		        $this->minMax->changementDe('jour');
		        $debug_message = $this->minMax->getDebugMessage();
		        if (count($debug_message) > 0 and $this->verbose === true) {
		            Util::log($this->log_file, implode("\n", $debug_message), __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
		        }
		    }
		    if ($this->flag_nouvelle_minute === true) {
				$this->writeCsvFileMinute($date_time);
		    }
		    if ($different_than_last_time === true) {
		    	// on écrit les mesures seconde uniquement si elles sont différentes des mesures précédentes
				$this->writeCsvFileSeconde($date_time);
		    }
			// réinitialisation pour la prochaine boucle
			$this->prepareForNextLoop();
		}
	}

	/** Initialisation des arguments du batch */
	private function checkArguments(){
		if($GLOBALS['argc'] < 2){
		    $log = "USAGE : ".$GLOBALS['argv'][0]." --display | --csv\n";
		    echo $log."\n";
		    Util::log($this->log_file, $log, __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
		    exit;
		}
		if(array_search('--csv', $GLOBALS['argv']) !== false){
		    $this->output_mode = 'csv';
		}else{
		    $this->output_mode = 'display';
		}
	}

	/** initialisation de la connexion à owserver */
	private function initOwServerConnexion(){		
		$nb_tentative_connexion = 3;
		while(true){
		    try {
		        $this->owServer = new OwSensors($this->owConfig);
		    } catch (Exception $e) {
		    	if ($this->output_mode == 'display') {
		    		echo $e->getMessage()."\n";
		    		exit;
		    	}
		        if ($nb_tentative_connexion === 1) {
		            Util::log($this->log_file, "Échec définitif de la connexion à owserver", __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
		            throw new OwSensorsConnexionException($e->getMessage());
		        }
		        sleep(10);
		        $nb_tentative_connexion--;
		        Util::log($this->log_file, "Échec de la connexion à owserver. Encore $nb_tentative_connexion tentatives", __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
		        continue;
		    }
		    break;
		}
		Util::log($this->log_file, "Connexion à owserver réussie.", __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
	}


	/** initialisation des minmax */
	private function initMinMax(){
		if($this->output_mode != 'csv'){
			return false;
		}
		$this->minMax = new MinMax(
		    $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_minmax_heure,
		    $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_minmax_today,
		    $this->owConfig->dir_output_mesure.'/'.$this->owConfig->fichier_minmax_yesterday
		);
		$debug_message = $this->minMax->getDebugMessage();
		if (count($debug_message) > 0) {
		    Util::log($this->log_file, implode("\n", $debug_message), __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
		}
	}


	/** Initialisation des fichiers CSV stockant les mesures */
	private function initCsvFIle(){
		if($this->output_mode != 'csv'){
			return false;
		}
	    $this->entete_csv = array('Date');
	    $this->mesure_null_heure = $this->mesure_null = array($this->owConfig->getTime());
	    foreach ($this->owServer as $cur_owSensor) {
	        $this->entete_csv[] = $cur_owSensor->getAlias();
	        $this->mesure_null[] = null; //'Nan'; // c'est la fonction MesureCsv::fwrite qui va s'occuper de la transformation en "Nan"
	        $this->mesure_null_heure[] = 'Nan;Nan;Nan'; // Comme le format est complexe, on s'occupe dès maintenant de mettre en place le format "Nan"
	    }
	    clearstatcache();
	    // Fichier SECONDE
	    if(file_exists($this->fichier_mesure_seconde) === false or filesize($this->fichier_mesure_seconde) === 0){
	        if (MesureCsv::fwrite($this->fichier_mesure_seconde, $this->entete_csv) === false) {
	            $log = "Impossible d'ouvrir le fichier csv ".$this->fichier_mesure_seconde;
	            Util::log($this->log_file, $log, __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	            throw new CantWriteFileException($log);
	        }
	    }else{
	        // On ajoute des valeurs null pour avoir un "trou" dans le graph seconde si les dernièers données sont "vieilles" (1 minute)
	        if ((time()-60) > MesureCsv::getTimestampFromLastLine($this->fichier_mesure_seconde)) {
	            MesureCsv::fwrite($this->fichier_mesure_seconde, $this->mesure_null);
	        }
	    }
	    // Fichier MINUTE
	    if(file_exists($this->fichier_mesure_minute) === false or filesize($this->fichier_mesure_minute) === 0){
	        if (MesureCsv::fwrite($this->fichier_mesure_minute, $this->entete_csv) === false) {
	            $log = "Impossible d'ouvrir le fichier csv ".$this->fichier_mesure_minute;
	            Util::log($this->log_file, $log, __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	            throw new CantWriteFileException($log);
	        }
	    }else{
	        // On ajoute des valeurs null pour avoir un "trou" dans le graph minute si les dernièers données sont "vieilles" (1 grosse minute)
	        if ((time()-70) > MesureCsv::getTimestampFromLastLine($this->fichier_mesure_minute)) {
	            MesureCsv::fwrite($this->fichier_mesure_minute, $this->mesure_null);
	        }
	    }
	    // Fichier HEURE
	    if(file_exists($this->fichier_mesure_heure) === false or filesize($this->fichier_mesure_heure) == 0){
	        if (MesureCsv::fwrite($this->fichier_mesure_heure, $this->entete_csv) === false) {
	            $log = "Impossible d'ouvrir le fichier csv ".$this->fichier_mesure_heure;
	            Util::log($this->log_file, $log, __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	            throw new CantWriteFileException($log);
	        }
	    }else{
	        // On ajoute des valeurs null pour avoir un "trou" dans le graph heure si les dernièers données sont "vieilles" (1 heure)
	        if ((time()-3600) > MesureCsv::getTimestampFromLastLine($this->fichier_mesure_heure)) {
	            MesureCsv::fwrite($this->fichier_mesure_heure, $this->mesure_null_heure);
	        }
	    }
	    return true;
	}

	/** resize fichier seconde */
	private function resizeFileSeconde(){
		if($this->output_mode != 'csv'){
			return false;
		}
	    if($this->nb_mesure_avant_resize_fichier_seconde <= 0){
	        $ret_resize_fichier = MesureCsv::resizeFile(
	            $this->fichier_mesure_seconde,
	            $this->owConfig->max_line_fichier_mesure_seconde, 
	            $this->entete_csv
	        );
	        if($ret_resize_fichier === true){
	            Util::log($this->log_file, "Redimmensionnement du fichier ".$this->fichier_mesure_seconde." par suppression des vieilles mesures (saut de ".$this->owConfig->freq_analyse_file_size.")", __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
	            $this->nb_mesure_avant_resize_fichier_seconde = $this->owConfig->freq_analyse_file_size;
	        }else{
	            Util::log($this->log_file, "Aucun redminensionnement du fichier ".$this->fichier_mesure_seconde." (saut de ".$ret_resize_fichier.")", __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
	            $this->nb_mesure_avant_resize_fichier_seconde = $ret_resize_fichier; // on augmente la boucle pour atteindre directement le nombre de ligne cible
	        }
	    }
	    $this->nb_mesure_avant_resize_fichier_seconde--;
	}

	/** resize fichier minute */
	private function resizeFileMinute(){
		if($this->output_mode != 'csv'){
			return false;
		}
	    if($this->nb_mesure_avant_resize_fichier_minute <= 0){
	        $ret_resize_fichier = MesureCsv::resizeFile(
	            $this->fichier_mesure_minute,
	            $this->owConfig->max_line_fichier_mesure_minute, 
	            $this->entete_csv
	        );
	        if($ret_resize_fichier === true){
	            Util::log($this->log_file, "Redimmensionnement du fichier ".$this->fichier_mesure_minute." par suppression des vieilles mesures (saut de ".$this->owConfig->freq_analyse_file_size.")", __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
	            $this->nb_mesure_avant_resize_fichier_minute = $this->owConfig->freq_analyse_file_size;
	        }else{
	            Util::log($this->log_file, "Aucun redminensionnement du fichier ".$this->fichier_mesure_minute." (saut de ".$ret_resize_fichier.")", __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
	            $this->nb_mesure_avant_resize_fichier_minute = $ret_resize_fichier; // on augmente la boucle pour atteindre directement le nombre de ligne cible
	        }
	    }
	    $this->nb_mesure_avant_resize_fichier_minute--;
	}

	/**
	 * récupération des mesures
	 *
	 * gère l'absence de mesure en ajoutant des "trous"
	 *
	 * @param BatchMesure::CHANGEMENT_SECONDE | BatchMesure::CHANGEMENT_JOURNEE | BatchMesure::CHANGEMENT_HEURE | BatchMesure::CHANGEMENT_MINUTE $flag_changement_periode détermine quelle période change.
	 * @return boolean true si les nouvelles mesures sont différentes des précédente sinon false
	 */
	private function getMesure($flag_changement_periode){
		$mesure_precedentes = $this->mesures_courantes;
	    $this->mesures_courantes = array();
	    foreach ($this->owServer as $cur_owSensor) {
	        if($this->output_mode == 'display'){
				$mesure = $cur_owSensor->getMesure();
		        if (is_array($mesure)) {
		            foreach ($mesure as $key => $value) {
		                $s_mesure .= "$value ($key), ";
		            }
		        }else{
		            $s_mesure = $mesure;
		        }
	            echo "\t- ".$s_mesure." ".$cur_owSensor->getUniteFull()."\t(".$cur_owSensor->getAlias()."\t".$cur_owSensor->getFullId().")\n";
	        }else{
	            try {
            		$this->mesures_courantes[$cur_owSensor->getAlias()] = $cur_owSensor->getMesure($flag_changement_periode);
	            } catch (OwSensorsConnexionException $e) {
	                $this->pb_connexion++;
	                switch ($this->pb_connexion) {
	                    case 1:
	                        Util::log($this->log_file, "Échec de la connexion à owserver.", __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
	                        $this->date_pb_connexion = time();
	                        break;
	                    case 2:
	                        Util::log($this->log_file, "En attente du rétablissement de la connexion à owserver.", __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	                        break;
	                    case 6:
	                        Util::log($this->log_file, "Ajout de valeur \"Nan\" dans le fichier ".$this->fichier_mesure_seconde." pour signifier l'absence de mesure", __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	                        // On ajoute des valeurs null pour avoir un "trou" dans le graph seconde si les dernièers données sont "vieilles" (une minute)
	                        $this->mesure_null[0] = $this->owConfig->getTime();
	                        MesureCsv::fwrite($this->fichier_mesure_seconde, $this->mesure_null);
	                        Util::log($this->log_file, "Passage en mode \"veille\" en attendant le rétablissement de la connexion", __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	                        break;
	                    case ($this->pb_connexion % 60 === 0):
	                        Util::log($this->log_file, "Connexion en attente de rétablissement", __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	                        break;
	                    case 36: // environ 1 heure
	                        // On ajoute des valeurs null pour avoir un "trou" dans le graph heure si les dernièers données sont "vieilles" (une heure)
	                        Util::log($this->log_file, "Ajout de valeur \"Nan\" dans le fichier ".$this->fichier_mesure_heure." pour signifier l'absence de mesure", __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
	                        $this->mesure_null_heure[0] = $this->owConfig->getTime();
	                        MesureCsv::fwrite($this->fichier_mesure_heure, $this->mesure_null_heure);
	                        break;
	                }
	                // on saute cette série de mesure en raison du problème de connexion au serveur
	                break;
	            } catch (Exception $e){
	                throw $e;
	            }
	            if ($this->pb_connexion >= 1) {
	                Util::log($this->log_file, "Connexion rétablie. La coupure a durée ".(round((time()-$this->date_pb_connexion)/60))." minutes. On reprend la boucle de mesure.", __FILE__, __LINE__, __METHOD__, Util::LOG_IMPORTANT);
	                $this->pb_connexion = 0;
	            }
	        }
	    }
	    if($this->output_mode == 'display'){
	    	// pas de boucle pour le mode display
	        exit;
	    }
	    if ($this->pb_connexion >= 1) {
	        // pb de connexion, on dort et on saute cette série de mesure en raison du problème de connexion au serveur
	        // en espérant que cela sera mieux dans les prochaines séries
	        if ($this->pb_connexion >= 6) {
	        	// le problème de connexion est persistent, on dort plus longtemps
	            sleep(110);
	        }
	        sleep($this->owConfig->daemon_mesure_frequence);
	        continue 1;
	    }
	    if ($mesure_precedentes === $this->mesures_courantes) {
	    	return false;
	    }
	    return true;
    }

    /** mise à jour des flags "horaires" (nouvelle minute, nouvelle heure et nouvelle journee) */
	private function checkTimeChanges($date_time){
		if($this->output_mode != 'csv'){
			return false;
		}
		// $date_time n'est pas utilisé pour détecter les changements horaires mais ce serait mieux.
	    if($this->minute_precedente === null){
	        $this->minute_precedente = date("i");
	    }
	    $this->minute_courante = date("i");
	    if ($this->minute_precedente != $this->minute_courante) {
	        $this->flag_nouvelle_minute = true;
	        $this->minute_precedente = $this->minute_courante;
	    }
	    if($this->heure_precedente === null){
	        $this->heure_precedente = date("G");
	    }
	    $this->heure_courante = date("G");
	    if ($this->heure_precedente != $this->heure_courante) {
	        $this->flag_nouvelle_heure = true;
	        if ($this->heure_courante === '0') {
	            $this->flag_nouvelle_journee = true;
	        }
	        $this->heure_precedente = $this->heure_courante;
	    }		
	}

	/** maj des min et des max */
	private function updateMinMax($date_time){
		$ret_minmax_update = $this->minMax->update($this->mesures_courantes, $date_time);
	    $debug_message = $this->minMax->getDebugMessage();
	    if (count($debug_message) > 0 and $this->verbose === true) {
	        Util::log($this->log_file, implode("\n", $debug_message), __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
	    }

	}

    /** ÉCRITURE des mesures HEURE et gestion des minmax associés (mode CSV uniquement) */
	private function writeCsvFileHeure($date_time){
		if($this->output_mode != 'csv' or $this->flag_nouvelle_heure != true){
			return false;
		}
        // pour les heures, voici le format :"2,10;25;35,20;10;25\n", { customBars: true });
        $mesures_courantes_heures = array();
        foreach ($this->mesures_courantes as $cur_mesure_name => $cur_mesure) {
        	if (is_array($cur_mesure) === true) {
        		// le sensor a retourné plusieurs valeur en fonction de la période
    			$cur_mesure = $cur_mesure[BatchMesure::CHANGEMENT_HEURE];
        	}
            // on encadre la mesure par la valeur min et max calculée sur les 60 minutes qui précédent
            $mesures_courantes_heures[$cur_mesure_name] = 
                $this->minMax->get('heure', 'min', $cur_mesure_name).';'.
                $cur_mesure.';'.
                $this->minMax->get('heure', 'max', $cur_mesure_name);
                $debug_message = $this->minMax->getDebugMessage();
                if (count($debug_message) > 0 and $this->verbose === true) {
                    Util::log($this->log_file, implode("\n", $debug_message), __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
                }
        }
        if (MesureCsv::fwrite($this->fichier_mesure_heure, array_merge(array($date_time), $mesures_courantes_heures)) === false) {
            $log = "Impossible d'ouvrir le fichier csv ".$this->fichier_mesure_heure;
            Util::log($this->log_file, $log, __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
            throw new CantWriteFileException($log);
        }
        // gestion des minmax : Nouvelle heure donc réinitialisation des minmax on prend donc les mesures courantes pour les min max heure
        $this->minMax->changementDe('heure');
        $debug_message = $this->minMax->getDebugMessage();
        if (count($debug_message) > 0 and $this->verbose === true) {
            Util::log($this->log_file, implode("\n", $debug_message), __FILE__, __LINE__, __METHOD__, Util::LOG_INFO);
        }
	}

    /** ÉCRITURE des mesures MINUTE (mode CSV uniquement) */
	private function writeCsvFileMinute($date_time){
		if($this->output_mode != 'csv' or $this->flag_nouvelle_minute != true){
			return false;
		}
		$mesures_courantes_minutes = $this->mesures_courantes;
        foreach ($this->mesures_courantes as $cur_mesure_name => $cur_mesure) {
        	if (is_array($cur_mesure) === true) {
        		// le sensor a retourné plusieurs valeur en fonction de la période
    			$mesures_courantes_minutes[$cur_mesure_name] = $cur_mesure[BatchMesure::CHANGEMENT_MINUTE];
        	}
        }
        if (MesureCsv::fwrite($this->fichier_mesure_minute, array_merge(array($date_time), $mesures_courantes_minutes)) === false) {
            $log = "Impossible d'ouvrir le fichier csv ".$this->fichier_mesure_minute;
            Util::log($this->log_file, $log, __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
            throw new CantWriteFileException($log);
        }
	}

    /** ÉCRITURE des mesures SECONDE en fonction du mode */
	private function writeCsvFileSeconde($date_time){
		if($this->output_mode != 'csv'){
			return false;
		}
        // fichier SECONDE
		$mesures_courantes_secondes = $this->mesures_courantes;
        foreach ($this->mesures_courantes as $cur_mesure_name => $cur_mesure) {
        	if (is_array($cur_mesure) === true) {
        		// le sensor a retourné plusieurs valeur en fonction de la période
    			$mesures_courantes_secondes[$cur_mesure_name] = $cur_mesure[BatchMesure::CHANGEMENT_MINUTE];
        	}
        }
        if (MesureCsv::fwrite($this->fichier_mesure_seconde, array_merge(array($date_time), $mesures_courantes_secondes)) === false) {
            $log = "Impossible d'ouvrir le fichier csv ".$this->fichier_mesure_seconde;
            Util::log($this->log_file, $log, __FILE__, __LINE__, __METHOD__, Util::LOG_CRITICAL);
            throw new CantWriteFileException($log);
        }
	}

	/** pause entre 2 boucles */
	private function prepareForNextLoop(){
	    // reinitialisaiton des flags car ils ne sont utilisés qu'une seule fois à chaque changement "horaire" de la journée
	    $this->flag_nouvelle_minute = false;
	    $this->flag_nouvelle_heure = false;
	    $this->flag_nouvelle_journee = false;
	    sleep($this->owConfig->daemon_mesure_frequence); // boucle du daemon
	}
}
