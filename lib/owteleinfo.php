<?php
/**
 * r�cup�ration des donn�es de t�l�info
 * 
 * Les donn�es sont r�cup�r�es dans un fichier tempon et pas directement sur le port serie.
 * C'est un daemon qui r�alise cette partie
 *
 * Le dameon va s'occuper d'�crire dans ce fichier les donn�es du compteur au format json
 */
class OwTeleinfo{
	private $data = null;
	private $fichier_teleinfo_source = null;
	private $fichier_teleinfo_heure_previous = null;
	private $fichier_teleinfo_journee_previous = null;
	const FAKE_DATA = '{"PAPP":"","IINST":""}';

	public function __construct($owConfig){
		$this->fichier_teleinfo_source = $owConfig->dir_output_mesure.'/'.$owConfig->fichier_teleinfo_source;
		$this->fichier_teleinfo_heure_previous = $owConfig->dir_output_mesure.'/'.$owConfig->fichier_teleinfo_heure_previous;
		$this->fichier_teleinfo_journee_previous = $owConfig->dir_output_mesure.'/'.$owConfig->fichier_teleinfo_journee_previous;
		if (!file_exists($this->fichier_teleinfo_heure_previous)) {
			file_put_contents($this->fichier_teleinfo_heure_previous, "");
		}
		if (!file_exists($this->fichier_teleinfo_journee_previous)) {
			file_put_contents($this->fichier_teleinfo_journee_previous, "");
		}
		$this->loadTeleinfo();
	}
	private function loadTeleinfo(){
		if (!file_exists($this->fichier_teleinfo_source)) {
			throw new Exception("Error Reading teleinfo source file. THe file ".$this->fichier_teleinfo_source." does not exists!");
			;
			return;
		}
		$this->data = json_decode(file_get_contents($this->fichier_teleinfo_source), true);
	}
	/** r�cup�ration des donn�es issues de la teleinfo */
	public function get($key = BatchMesure::CHANGEMENT_SECONDE){
		$this->loadTeleinfo();
		$output = array();
		switch ($key) {

			case BatchMesure::CHANGEMENT_SECONDE :
			case BatchMesure::CHANGEMENT_MINUTE :
			case BatchMesure::CHANGEMENT_HEURE :
			case BatchMesure::CHANGEMENT_JOURNEE :
				$output[BatchMesure::CHANGEMENT_SECONDE] = $this->data['IINST'];
				if (is_null($this->data['IINST'])) {
					$output[BatchMesure::CHANGEMENT_SECONDE] = null;
				}

			case BatchMesure::CHANGEMENT_MINUTE :
			case BatchMesure::CHANGEMENT_HEURE :
			case BatchMesure::CHANGEMENT_JOURNEE :
				$output[BatchMesure::CHANGEMENT_MINUTE] = $this->data['IINST'];

			case BatchMesure::CHANGEMENT_HEURE :
			case BatchMesure::CHANGEMENT_JOURNEE :
				$teleinfo_heure_previous = json_decode(file_get_contents($this->fichier_teleinfo_heure_previous), true);
				if (is_null($this->data['PAPP']) or is_null($teleinfo_heure_previous['PAPP'])) {
					$output[BatchMesure::CHANGEMENT_HEURE] = null;
				}
				// on sauvegarde les donn�es courantes pour la prochaine heure
				file_put_contents($this->fichier_teleinfo_heure_previous, $this->data);
				$output[BatchMesure::CHANGEMENT_HEURE] = $this->data['PAPP'] - $teleinfo_heure_previous['PAPP'];

			case BatchMesure::CHANGEMENT_JOURNEE :
				$teleinfo_journee_previous = json_decode(file_get_contents($this->fichier_teleinfo_journee_previous), true);
				if (is_null($this->data['PAPP']) or is_null($teleinfo_journee_previous['PAPP'])) {
					$output[BatchMesure::CHANGEMENT_JOURNEE] = null;
				}
				// on sauvegarde les donn�es courantes pour la prochaine journee
				file_put_contents($this->fichier_teleinfo_journee_previous, $this->data);
				$output[BatchMesure::CHANGEMENT_JOURNEE] = $this->data['PAPP'] - $teleinfo_journee_previous['PAPP'];
		}
		// Si toutes les valeurs sont nulles ont retourne null
		$flag_all_null = true;
		foreach ($output as $value) {
			if ($value !== null) {
				$flag_all_null = false;
			}
		}
		if ($flag_all_null === true) {
			return null;
		}
		return $output;
	}
	public function set($path, $value=''){
		throw new BadMethodCallException("OwTeleinfo is a readonly sensor");
	}
}
