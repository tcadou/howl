#!/usr/bin/php -d magic_quotes_gpc=off
<?php
/**
 * récupère et stock les mesures des sondes toutes les OwConfig::daemon_mesure_frequence secondes
 *
 * Le script supporte deux modes, daemon (arg --csv) et unitaire (arg -display).
 * Dans le cadre du mode csv, plusieurs fichiers sont alimentés à des fréquences différentes.
 * 
 * Le script gère les mesures par :
 * - heure (fichier OwConfig::dir_output_mesure/OwConfig::fichier_mesure_heure)
 * - minute (fichier OwConfig::dir_output_mesure/OwConfig::fichier_mesure_minute)
 * - seconde (fichier OwConfig::dir_output_mesure/OwConfig::fichier_mesure_seconde)
 * 
 * L'absence de mesure pendant un certains se traduit par une insertion de valeur "Nan"
 * afin de créer un "trou" dans les graph.
 *
 * Les vielles mesures dans les fichiers minute et seconde sont régulièrement supprimées.
 *
 * Le csv heure est complété par des valeurs min et max de l'heure précédente en plus de la mesure courante.
 *
 * Le fichier de log du script se trouve là : $owConfig->dir_output_mesure/mesure.log
 *
 * 
 * ⃠ ☠ ☹ ⚐ ⚑     ✩ ✪ ✭
 */


/** Initialisation des variables */
require '/opt/howl/lib/common.php';

$owConfig = new OwConfig();
$log_file = str_replace('php', 'log', $owConfig->dir_output_mesure.'/'.basename(__FILE__));

$batchMesure = new BatchMesure($owConfig, $log_file);

$batchMesure->run();
