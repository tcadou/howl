#!/usr/bin/php -d magic_quotes_gpc=off
<?php
/**
 * récupère et stoque les heures de sunrise et sunset
 * Ce batch est à croner au plus tôt et au plus tard dans la journée afin de récupérer les données dans la majorité des cas
 */
$block_ownet_check = true; // désactive le check et l'inclusion de la lib ownet car elle n'est pas nécessaire pour ce programme
require '/opt/howl/lib/common.php';

// conf
$owConfig = new OwConfig();

$log_file = str_replace('json', 'log', $owConfig->dir_output_mesure.'/'.$owConfig->fichier_sunrisesunset);

Util::log($log_file, "#### récupération des heures sunrise et sunset", __FILE__, __LINE__, __METHOD__);

// récupération des arguments
if($argc < 2){
    $log = "USAGE : ".$argv[0]." <display|csv>\n";
    echo $log."\n";
    Util::log($log_file, "\t".$log, __FILE__, __LINE__, __METHOD__);
    exit;
}

if($argv[1] == 'csv'){
    $output_mode = 'csv';
}else{
    $output_mode = 'display';
}


$sunriseSunset = new SunriseSunset(
	$owConfig->location_sunrisesunset,
	$owConfig->dir_output_mesure.'/'.$owConfig->fichier_sunrisesunset
);
$debug_message = $sunriseSunset->getDebugMessage();
if (count($debug_message) > 0) {
    Util::log($log_file, "\t".implode("\n\t", $debug_message), __FILE__, __LINE__, __METHOD__);
	if ($output_mode == 'csv') {
		Util::log($log_file, "\t".implode("\n\t", $debug_message), __FILE__, __LINE__, __METHOD__);
	}else{
		echo implode("\n", $debug_message)."\n";
	}
}

$ret_loadCurrent = $sunriseSunset->loadCurrent();
$debug_message = $sunriseSunset->getDebugMessage();
if (count($debug_message) > 0) {
	if ($output_mode == 'csv') {
		Util::log($log_file, "\t".implode("\n\t", $debug_message), __FILE__, __LINE__, __METHOD__);
	}else{
		echo implode("\n", $debug_message)."\n";
	}
}
if ($ret_loadCurrent === false) {
	// aucune donnée à sauvegarder
	if($output_mode != 'csv'){
		echo "Aucune pour aujourd'hui !\n";
	}
	exit;
}

if($output_mode == 'csv'){
	$sunriseSunset->save();
	$debug_message = $sunriseSunset->getDebugMessage();
	if (count($debug_message) > 0) {
	    Util::log($log_file, "\t".implode("\n\t", $debug_message), __FILE__, __LINE__, __METHOD__);
	}
}else{
		echo "### Données pour aujourd'hui :\n";
		echo "\t- date    : ".$sunriseSunset->getCurrentDate()."\n";
		echo "\t- sunrise :  ".$sunriseSunset->getCurrentSunrise()."\n";
		echo "\t- sunset  : ".$sunriseSunset->getCurrentSunset()."\n";
		echo implode("\n", $sunriseSunset->getDebugMessage())."\n";

		echo "### Données des 4 derniers jours :\n";
		echo $sunriseSunset->load(4)."\n";
		echo implode("\n", $sunriseSunset->getDebugMessage())."\n";
}

