Un peu de doc :


projet house-on-wire en version light : http://code.google.com/p/houseonwire


# Structure de l'IHM :
    - Page 1 : VALEUR NOW    : à partir du fichier mesure_seconde sur une même page avec des codes couleurs, pour chaque sonde :
        - la valeur numérique courante (dernière ligne du fichier seconde si non périmée)
        - la valeur min sur aujourd'hui (on récupère dans minMaxJourToday)
        - la valeur max sur aujourd'hui (on récupère dans minMaxJourToday)
        - la valeur min sur hier (on récupère dans minMaxJourYesterday)
        - la valeur max sur hier (on récupère dans minMaxJourYesterday)
        - l'écart entre la valeur il y a 24 heure à la même heure. On retourne la ligne n°(FILE_NB_LINE - 1440). @see http://us1.php.net/manual/fr/splfileobject.seek.php
        - l'écart entre la valeur il y a 1 semaine à la même heure. On retourne la ligne n°(FILE_NB_LINE - 10080).  @see http://us1.php.net/manual/fr/splfileobject.seek.php
    - Page 2 : GRAPH MINUTE  : à partir du fichier mesure_minute (15 jours de données).
    - Page 3 : GRAPH ANNÉE   : à partir du fichier mesure_heure (données sans limite).
    - page 4 : GRAPH SECONDE : à partir du fichier mesure_seconde (2 jours de données).


# Architecture de l'application howl (houseonwire-light)
    - toutes les secondes (DAEMON SECONDE) on stock les mesures dans le fichier "secondes" en interrogeant owserver au travers d'un daemon.
        * Toutes les n mesures, on supprime des lignes en début de fichier pour avoir au maximum 15000 lignes (en pratique, nous avons 1 mesure toutes les 10-11 secondes soit 7 500 points par jour).
    - toutes les secondes (DAEMON SECONDE) on met à jour, si besoin, le fichier "minMaxHeure". On flush le fichier toutes les heures
    - toutes les secondes (DAEMON SECONDE) on met à jour, si besoin, le fichier "minMaxToday". A minuit, on déplace "minmaxtoday" dans "minmaxyesterday".
    - toutes les minutes (DAEMON SECONDE) on stock la dernière ligne du fichier "seconde" dans le fichier "minutes". On garde 15 jours de mesure  (60*24*15 = 21 600 points).
        * Toutes les n mesures, on supprime des lignes en début de fichier pour avoir au maximum 21 600 lignes - 60(mesureParHeure) * 24(heure) * 15(jour).
    - toutes les heures (DAEMON SECONDE) on stock la mesure dans le fichier "heure" on y ajoute les valeurs du fichier "minMaxHeure". On ne flush jamais ce fichier (24*365 = 8 760 points par an).
    - le serveur web est intégré au script d'install, on utilise le serveur web intégré de php.
# backup :
	rsync -Laz pi@howl.local:/etc/howl_config_perso.php /Users/tdecadoudal/dev/perso/howl-backup-pi/config/ ; rsync -Laz pi@howl.local:/opt/howl /Users/tdecadoudal/dev/perso/howl-backup-pi/opt/ ; rsync -Laz pi@howl.local:/var/log/howl /Users/tdecadoudal/dev/perso/howl-backup-pi/log/
# Installation :
	- livraison sur le PI : 
		rsync -az /Users/tdecadoudal/dev/perso/houseonwire-light/ pi@howl.local:/home/pi/houseonwire
    - install.sh (sans argument pour un peu de doc)
    - php 5.3 ou >
# Désinstallation :
    - uninstall.sh
# Info sur le module OneWire
    - voir doc/INFO.txt
