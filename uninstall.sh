#!/bin/bash

ISROOT=`whoami`
if [ "root" != "$ISROOT" ]
then
  echo "Vous n'êtes qu'un utilisateur ordinaire (mais maman vous aime quand même). Vous devez lancer ce script en tant que root."
  exit 0
fi


if [ ! -n "$1" ]
then
  echo "Usage: `basename $0` OK"
  echo -e "\t L'ensemble des fichiers et daemon seront supprimés a l'exception de :"
  echo -e "\t - /var/log/howl/*.csv"
  echo -e "\t - les fichiers sources du programme"
  echo -e "\t - la config personnalisées définie par \$HOWL_CONFIG : $HOWL_CONFIG"
  exit 1
fi

SRC=`dirname $0`
cd $SRC
SRC=`pwd`

echo ">> arrêts des daemon :"
rm /etc/supervisor/conf.d/howl-supervisord.conf
supervisorctl update
/etc/init.d/owserver stop

echo ">> suppression du module teleinfo :"
pip uninstall teleinfo --yes

echo ">> suppression de owserver :"
apt-get remove owfs -y
rm /etc/owfs.conf

echo ">> suppression des fichiers du programme :"
rm -rf /opt/howl /var/www
rm /etc/cron.d/howl_sunrisesunset

echo ">> désinstallation terminée !"
echo ">> Les seuls fichiers encore en vie sont :"
echo -e "\t - /var/log/howl/*.csv et /var/log/howl/*.log"
echo -e "\t - les fichiers sources du programme"
echo -e "\t - la config personnalisées définie par \$HOWL_CONFIG : $HOWL_CONFIG"
