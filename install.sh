#!/bin/bash

ISROOT=`whoami`
if [ "root" != "$ISROOT" ]; then
  echo "Vous n'êtes qu'un utilisateur ordinaire (mais maman vous aime quand même). Vous devez lancer ce script en tant que root."
  exit 0
fi


if [ ! -n "$1" ]; then
  echo "Usage: `basename $0` OK"
  echo -e "\t Des liens ou des fichiers seront créés vers les répertoires suivants :"
  echo -e "\t - /opt/howl/"
  echo -e "\t - /var/www/howl/"
  echo -e "\t - /var/log/howl/"
  echo -e "\t - /etc/cron.d/"
  exit 1
fi

SRC=`dirname $0`
cd $SRC
SRC=`pwd`

echo ">> On commence par faire du ménage :"
./uninstall.sh OK

echo ">> Préparation de la teleinfo : (il faut rebooter après la toute première installation)"
sed -i -e 's@console=ttyAMA0,115200@ @' /boot/cmdline.txt
sed -i -e 's@kgdboc=ttyAMA0,115200@ @' /boot/cmdline.txt
sed -i -e '/^[^#].*getty -L ttyAMA0/ s/^/#/' /etc/inittab

echo ">> Création des répertoires cibles :"
mkdir -p /opt/howl
if [[ ! -e /var/log/howl ]]; then
	mkdir -p /var/log/howl
fi
mkdir -p /var/www

echo ">> Installation de owfs :"
apt-get install owfs
cp $SRC/install/etc/owfs.conf /etc/
# on test si on utilise le port usb ou i2c pour le module onewire
ls /dev/ttyUSB0 > /dev/null 2>&1
if [ $? -eq 0 ]; then
  sed -i -e 's@#server: device = /dev/ttyUSB0@server: device = /dev/ttyUSB0@' /etc/owfs.conf
  sed -i -e 's@#server: device = /dev/i2c-1@#server: device = /dev/i2c-1@' /etc/owfs.conf
fi

echo ">> Installation de supervisor :"
apt-get -y install supervisor

echo ">> Installation du module teleinfo :"
apt-get install python-pip
pip install teleinfo

echo ">> Création des liens et des fichiers de l'application :"
ln -s $SRC/batch /opt/howl/batch
ln -s $SRC/lib /opt/howl/lib
ln -s $SRC/config /opt/howl/config
ln -s $SRC/www /var/www/howl

echo ">> Installation de localtunnel.me"
which node > /dev/null
if [ $? -eq 1 ]; then
	#wget http://node-arm.herokuapp.com/node_latest_armhf.deb -o /tmp/node_latest_armhf.deb
	wget https://nodejs.org/dist/v6.11.2/node-v6.11.2-linux-armv6l.tar.xz -P /tmp/
	tar -xJ -C /tmp/ -f /tmp/node-v6.11.2-linux-armv6l.tar.xz

	#mv /tmp/node-v6.11.2-linux-armv6l /opt/nodejs
	#ln -s /opt/nodejs/bin/node /usr/bin/node
	#ln -s /opt/nodejs/bin/npm /usr/bin/npm

	#OU
	cp -R /tmp/node-v6.11.2-linux-armv6l/bin /usr/local/
	cp -R /tmp/node-v6.11.2-linux-armv6l/include /usr/local/
	cp -R /tmp/node-v6.11.2-linux-armv6l/lib /usr/local/
	cp -R /tmp/node-v6.11.2-linux-armv6l/share /usr/local/
fi
npm install -g localtunnel

echo ">> Intégration des service dans supervisor et lancement des démons :"
cp $SRC/install/etc/supervisor/conf.d/howl-supervisord.conf /etc/supervisor/conf.d/howl-supervisord.conf
supervisorctl update
service owserver start

echo ">> Installation de howl_sunrisesunset :"
cp $SRC/install/etc/cron.d/howl_sunrisesunset /etc/cron.d/howl_sunrisesunset

echo ">> Lancement des daemon :"
service cron reload


echo ""
if [ ! -e "/etc/howl_config_perso.php" ]; then
  echo "Vous pouvez définir votre propre fichier de config à partir du modèle 'owconfig_modele.php'"
  echo "et déposer ce fichier là : /etc/howl_config_perso.php"
else
  echo "Un fichier de config personnalisé se trouve là : /etc/howl_config_perso.php"
fi

